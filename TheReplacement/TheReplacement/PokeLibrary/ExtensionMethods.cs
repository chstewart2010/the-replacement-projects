﻿using Newtonsoft.Json.Linq;
using System;
using TheReplacement.PokeLibrary.Battle.Field.Enums;
using TheReplacement.PokeLibrary.Pokeball.Enum;
using TheReplacement.PokeLibrary.Pokemon.PokemonParts;
using TheReplacement.PokeLibrary.Trainer;

namespace TheReplacement.PokeLibrary
{
    public static class ExtensionMethods
    {
        public static Types[] GetResistances(this Types type)
        {
            TypesAttribute attribute = GetAttribute<TypesAttribute>(type, type.GetType());
            return attribute.Resistances;
        }

        public static Types[] GetWeakness(this Types type)
        {
            TypesAttribute attribute = GetAttribute<TypesAttribute>(type, type.GetType());
            return attribute.Weaknesses;
        }

        public static Types[] GetImmuniites(this Types type)
        {
            TypesAttribute attribute = GetAttribute<TypesAttribute>(type, type.GetType());
            return attribute.Immunities;
        }

        public static double GetStatusMultipler(this Status status)
        {
            StatusModifierAttribute attribute = GetAttribute<StatusModifierAttribute>(status, status.GetType());
            return attribute == null
                ? 1
                : attribute.StatusMultiplier;
        }

        public static double GetBallMultipler(this Pokeballs ball, bool isUltra, uint opposingLevel = 100, uint yourLevel = 1, int turn = 0)
        {
            BallAttribute attribute = GetAttribute<BallAttribute>(ball, ball.GetType());

            if (isUltra)
            {
                return ball switch
                {
                    Pokeballs.BeastBall => attribute.BallMultiplier,
                    _ => .1
                };
            }
            return ball switch
            {
                Pokeballs.LevelBall => yourLevel >= 4 * opposingLevel
                        ? 8
                        : yourLevel > 2 * opposingLevel
                            ? 4
                            : yourLevel > opposingLevel
                                ? 2
                                : 1,
                Pokeballs.NestBall => opposingLevel < 30
                        ? (41 - opposingLevel) / 10
                        : 1,
                Pokeballs.TimerBall => turn < 10
                        ? 1 + turn * 1229 / 4096
                        : 4,
                Pokeballs.QuickBall => turn == 0
                        ? 5
                        : 1,
                _ => attribute.BallMultiplier
            };
        }

        public static double GetFailMultipler(this Pokeballs ball, bool isUltra)
        {
            BallAttribute attribute = GetAttribute<BallAttribute>(ball, ball.GetType());
            return isUltra ? .1 : attribute.FailMultiplier;
        }

        public static double GetExpNeeded(this ExperienceGrowthRate rate, uint level = 0)
        {
            if (level == 100)
                level--;
            double next = level + 1;
            double temp = Math.Pow(next, 3);
            return rate switch
            {
                ExperienceGrowthRate.Erratic => level <= 50
                                       ? temp * Math.Floor(Math.Floor((100 - next) / 50))
                                       : level <= 68
                                           ? temp * Math.Floor(Math.Floor((150 - next) / 100))
                                           : level <= 98
                                               ? temp * Math.Floor(Math.Floor((1911 - 10 * next) / 3) / 50)
                                               : temp * Math.Floor(Math.Floor((160 - next) / 100)),
                ExperienceGrowthRate.Fast => Math.Floor(4 * temp / 5),
                ExperienceGrowthRate.MediumFast => temp,
                ExperienceGrowthRate.MediumSlow => Math.Floor(6 * temp / 5 - 15 * Math.Pow(next, 2) + 100 * next - 140),
                ExperienceGrowthRate.Slow => Math.Floor(5 * temp / 4),
                ExperienceGrowthRate.Fluctuating => level <= 15
                    ? temp * Math.Floor((Math.Floor((next + 1) / 3) + 24) / 50)
                    : level <= 36
                        ? Math.Floor(temp * (next + 14) / 50)
                        : temp * Math.Floor((Math.Floor(next / 2) + 32) / 50),
                _ => -1,
            };
        }

        public static string GetName(this TrainerTypes type)
        {
            TrainerTypeAttribute attribute = GetAttribute<TrainerTypeAttribute>(type, type.GetType());
            return attribute == null
                ? type.ToString()
                : attribute.Name;
        }

        public static string GetURL(this Weather weather)
        {
            WeatherAttribute attribute = GetAttribute<WeatherAttribute>(weather, weather.GetType());
            return attribute.URL;
        }

        public static string Capitalize(this string myString)
        {
            string temp = string.Empty;
            foreach (string split in myString.Split(' '))
            {
                char firstChar = split[0];
                string capitalize = firstChar.ToString().ToUpper();
                temp += capitalize + split.Substring(1) + " ";
            }

            return temp.Trim();
        }

        internal static TEnum ToEnum<TEnum>(this string myEnum) where TEnum : Enum
        {
            return (TEnum)Enum.Parse(typeof(TEnum), myEnum.Replace(" ", ""));
        }

        internal static string GetStringFromToken(this JToken token, string key = null)
        {
            return string.IsNullOrEmpty(key)
                ? (string)token
                : token[key] == null
                    ? ""
                    : (string)token[key];
        }

        internal static string GetStringFromToken(this JToken token, int key)
        {
            return token[key] == null
                ? ""
                : (string)token[key];
        }

        internal static uint GetIntFromToken(this JToken token, string key = null)
        {
            return token == null
                ? 0
                : string.IsNullOrEmpty(key)
                    ? (uint)token
                    : token[key] == null
                        ? 0
                        : (uint)token[key];
        }

        internal static int GetIntFromToken(this JToken token, int key)
        {
            return token[key] == null
                ? 0
                : (int)token[key];
        }

        internal static bool GetBoolFromToken(this JToken token, string key = null)
        {
            return string.IsNullOrEmpty(key)
                ? (bool)token
                : token[key] == null
                    ? false
                    : (bool)token[key];
        }

        internal static bool GetBoolFromToken(this JToken token, int key)
        {
            return token[key] == null
                ? false
                : (bool)token[key];
        }

        private static T GetAttribute<T>(object enumName, Type enumType) where T : Attribute
        {
            var attributes = enumType.GetField(enumName.ToString()).GetCustomAttributes(typeof(T), false);
            return attributes == null
                ? null
                : (T)attributes[0];
        }
    }
}
