﻿using Newtonsoft.Json.Linq;
using System.IO;
using System.Net;
using System.Text;

namespace TheReplacement.PokeLibrary
{
    public static class PokeAPI
    {
        /// <summary>
        /// Returns a json object with information regarding a pokemon species
        /// </summary>
        /// <param name="species">Pokemon species</param>
        public static JObject GetSpecies(string species) => GetResponseFromPokeAPI("pokemon-species", species);

        /// <summary>
        /// Returns a json object with information regarding a pokemon form
        /// </summary>
        /// <param name="pokemon">Pokemon form</param>
        public static JObject GetForm(string pokemon) => GetResponseFromPokeAPI("pokemon", pokemon);

        /// <summary>
        /// Returns a json object with information regarding a pokemon's nature
        /// </summary>
        /// <param name="nature">One of the 24 pokemon natures</param>
        public static JObject GetNature(string nature) => GetResponseFromPokeAPI("nature", nature);

        /// <summary>
        /// Returns a json object with information regarding an ability
        /// </summary>
        /// <param name="ability">Pokemon ability</param>
        public static JObject GetAbility(string ability) => GetResponseFromPokeAPI("ability", ability);

        /// <summary>
        /// Returns a json object with information regarding a move
        /// </summary>
        /// <param name="move">Pokemon move</param>
        public static JObject GetMove(string move) => GetResponseFromPokeAPI("move", move);

        /// <summary>
        /// Returns a json object with information regarding an ttem
        /// </summary>
        /// <param name="item">Item that can be held</param>
        public static JObject GetItem(string item) => GetResponseFromPokeAPI("item", item);

        /// <summary>
        /// Returns a json object, hopefully
        /// </summary>
        /// <param name="url">request url</param>
        public static JObject GetResponse(string url)
        {
            HttpWebRequest request = WebRequest.CreateHttp(url);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            try
            {
                var response = request.GetResponse();
                string responseText;
                using (var reader = new StreamReader(response.GetResponseStream(), ASCIIEncoding.ASCII))
                    responseText = reader.ReadToEnd();
                return responseText == "Not Found" ? null : JObject.Parse(responseText);
            }
            catch (WebException)
            {
                return null;
            }
        }

        private static JObject GetResponseFromPokeAPI(string service, string item)
        {
            string url = $"https://pokeapi.co/api/v2/{service.ToLower().Replace(" ", "-")}/{item.ToLower().Replace(" ", "-")}";
            HttpWebRequest request = WebRequest.CreateHttp(url);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            try
            {
                var response = request.GetResponse();
                string responseText;
                using (var reader = new StreamReader(response.GetResponseStream(), ASCIIEncoding.ASCII))
                    responseText = reader.ReadToEnd();
                return responseText == "Not Found" ? null : JObject.Parse(responseText);
            }
            catch (WebException)
            {
                return null;
            }
        }
    }
}
