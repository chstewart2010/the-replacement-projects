﻿using System.Collections.Generic;
using TheReplacement.PokeLibrary.Battle.Field.Enums;

namespace TheReplacement.PokeLibrary.Battle.Field
{
    /// <summary>
    /// Defines the structure of the battle field
    /// </summary>
    public struct FieldStruct
    {
        private int _weatherCounter;
        private int _terrainCounter;

        public BattleStyle BattleStyle { get; }
        public Dictionary<Hazards,bool> Hazards { get; }
        public Location Location { get; }
        public Weather Weather { get; private set; }
        public Terrain Terrain { get; private set; }

        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="style"></param>
        /// <param name="location"></param>
        /// <param name="terrain"></param>
        /// <param name="weather"></param>
        public FieldStruct(BattleStyle style, Location location, Terrain terrain, Weather weather)
        {
            BattleStyle = style;
            Hazards = new Dictionary<Hazards, bool>();
            Location = location;
            Weather = weather;
            Terrain = terrain;
            _weatherCounter = 0;
            _terrainCounter = 0;
        }

        /// <summary>
        /// Increments the Weather and Terrain counters down, if applicable
        /// </summary>
        public void IncrementCountersDown()
        {
            if (_weatherCounter > 0)
            {
                _weatherCounter--;
                if (_weatherCounter == 0)
                {
                    Weather = Weather.None;
                }
            }
            if (_terrainCounter > 0)
            {
                _terrainCounter--;
                if (_terrainCounter == 0)
                {
                    Terrain = Terrain.None;
                }
            }
        }
    }
}
