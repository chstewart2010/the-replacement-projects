﻿namespace TheReplacement.PokeLibrary.Battle.Field.Enums
{
    /// <summary>
    /// Singles or Doubles Battle
    /// </summary>
    public enum BattleStyle
    {
        Singles = 1,
        Doubles = 2
    }
}
