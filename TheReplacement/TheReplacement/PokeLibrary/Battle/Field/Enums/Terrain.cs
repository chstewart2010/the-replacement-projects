﻿namespace TheReplacement.PokeLibrary.Battle.Field.Enums
{
    /// <summary>
    /// Defines the set of possible terrains
    /// </summary>
    public enum Terrain
    {
        None,
        ElectricTerrain,
        GrassyTerrain,
        PsychicTerrain,
        MistyTerrain
    }
}
