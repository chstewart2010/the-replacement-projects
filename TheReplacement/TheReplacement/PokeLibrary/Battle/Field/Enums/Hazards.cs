﻿using System.ComponentModel;

namespace TheReplacement.PokeLibrary.Battle.Field.Enums
{
    /// <summary>
    /// Defines the types of entry hazards
    /// </summary>
    public enum Hazards
    {
        [Description("Stealth Rocks")]
        StealthRocks = 0,

        [Description("One Layer of Spikes")]
        Spikes1 = 1,

        [Description("Two Layers of Spikes")]
        Spikes2 = 2,

        [Description("Three Layers of Spikes")]
        Spikes3 = 3,

        [Description("Steel Sturge")]
        SteelSturge = 4,

        [Description("One Layer of Toxic Spikes")]
        ToxicSpikes1 = 5,

        [Description("Two Layers of Toxec Spikes")]
        ToxicSpikes2 = 6,

        [Description("Sticky Web")]
        StickyWeb
    }
}