﻿namespace TheReplacement.PokeLibrary.Battle.Field.Enums
{
    /// <summary>
    /// Defines the possible battle locations
    /// </summary>
    public enum Location
    {
        Field,
        Cave,
        Surfing,
        Fishing,
        Underwater,
        Urban,
        Unusual
    }
}
