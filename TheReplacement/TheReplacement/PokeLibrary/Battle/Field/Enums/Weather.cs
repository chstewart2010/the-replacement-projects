﻿using System;

namespace TheReplacement.PokeLibrary.Battle.Field.Enums
{
    /// <summary>
    /// Defines a set of possible weathers
    /// </summary>
    public enum Weather
    {
        [Weather(false, "")]
        None,

        [Weather(false, "https://play.pokemonshowdown.com/fx/weather-sunnyday.jpg")]
        Sunny,

        [Weather(false, "https://play.pokemonshowdown.com/fx/weather-raindance.jpg")]
        Rain,

        [Weather(false, "https://play.pokemonshowdown.com/fx/weather-sandstorm.png")]
        Sandstorm,

        [Weather(false, "https://play.pokemonshowdown.com/fx/weather-hail.png")]
        Hail,

        [Weather(true, "https://play.pokemonshowdown.com/fx/weather-sunnyday.jpg")]
        HarshSun,

        [Weather(true, "https://play.pokemonshowdown.com/fx/weather-raindance.jpg")]
        HarshRain,

        [Weather(true, "")]
        HarshWind
    }

    public class WeatherAttribute : Attribute
    {
        public bool IsHarsh { get; }
        public string URL { get; }

        public WeatherAttribute(bool isHarsh, string url)
        {
            IsHarsh = isHarsh;
            URL = url;
        }
    }
}
