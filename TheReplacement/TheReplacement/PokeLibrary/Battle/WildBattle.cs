﻿using TheReplacement.PokeLibrary.Battle.Field.Enums;
using TheReplacement.PokeLibrary.Pokemon;
using TheReplacement.PokeLibrary.Trainer;

namespace TheReplacement.PokeLibrary.Battle
{
    /// <summary>
    /// Defines a battle between a trainer and a wild pokemon
    /// </summary>
    public class WildBattle : BattleBase<WildContainer, WildPokemon>
    {
        /// <summary>
        /// Constructor Method
        /// </summary>
        /// <param name="style"></param>
        /// <param name="location"></param>
        /// <param name="terrain"></param>
        /// <param name="weather"></param>
        public WildBattle(BattleStyle style, Location location, Terrain terrain, Weather weather) : base(style, location, terrain, weather)
        {

        }

        public void ExecuteCatchTurn()
        {

        }
    }
}
