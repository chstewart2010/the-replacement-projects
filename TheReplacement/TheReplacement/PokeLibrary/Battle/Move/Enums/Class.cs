﻿namespace TheReplacement.PokeLibrary.Battle.Move.Enums
{
    /// <summary>
    /// Classification of the Move
    /// </summary>
    public enum Class
    {
        Status,
        Physical,
        Special
    }
}
