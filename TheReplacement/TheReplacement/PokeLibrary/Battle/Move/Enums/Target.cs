﻿namespace TheReplacement.PokeLibrary.Battle.Move.Enums
{
    /// <summary>
    /// The move's intended target
    /// </summary>
    public enum Target
    {
        SpecificMove,
        SelectedPokemonMeFirst,
        Ally,
        UsersField,
        UserOrAlly,
        OpponentsField,
        User,
        Random,
        AllOtherPokemon,
        SelectedPokemon,
        AllOpponents,
        UserAndAllies,
        AllPokemon,
    }
}
