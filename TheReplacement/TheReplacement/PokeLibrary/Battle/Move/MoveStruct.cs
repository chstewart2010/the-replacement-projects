﻿using Newtonsoft.Json.Linq;
using System;
using TheReplacement.PokeLibrary.Battle.Move.Enums;
using TheReplacement.PokeLibrary.Pokemon.PokemonParts;

namespace TheReplacement.PokeLibrary.Battle.Move
{
    /// <summary>
    /// Defines the structure of the move
    /// </summary>
    public struct MoveStruct
    {
        private readonly JObject _move;


        public string Name { get; }
        public Class Class { get; }
        public Target Target { get; }
        public Types Type { get; }
        public int PPTotal { get; private set; }
        public int PPCurrent { get; private set; }

        /// <summary>
        /// Number of turns since calling the move to execute
        /// </summary>
        public int TurnCount { get; private set; }
        /// <summary>
        /// Number of turns required to execute the move
        /// </summary>
        public int TurnExecute { get; }
        public int Power { get; }
        
        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="move"></param>
        public MoveStruct(JObject move)
        {
            _move = move;
            Name = ((string)_move["name"]).Capitalize();
            Class = (Class)Enum.Parse(typeof(Class), ((string)_move["damage_class"]["name"]).Capitalize());
            Target = (Target)Enum.Parse(typeof(Target), ((string)_move["target"]["name"]).Replace("-", " ").Capitalize().Replace(" ", ""));
            Type = (Types)Enum.Parse(typeof(Types), ((string)_move["type"]["name"]).Capitalize());
            PPTotal = (int)_move["PP"];
            PPCurrent = PPTotal;
            TurnCount = 1;
            TurnExecute = 1;
            Power = _move["power"] == null
                ? 0
                : (int)_move["power"];
        }

        /// <summary>
        /// Returns an action signify what the pokemon will do
        /// </summary>
        public Action ExecuteMove()
        {
            Action action;
            if (TurnCount == TurnExecute)
            {
                action = null;
            }
            else
            {
                TurnCount++;
                action = null;
            }

            return action;
        }

        public bool IncrementPP(bool up)
        {
            if (up)
            {
                bool value = PPCurrent < PPTotal;
                if (value)
                {
                    PPCurrent++;
                }
                return value;
            }
            else
            {
                bool value = PPCurrent > 0;
                if (value)
                {
                    PPCurrent--;
                }
                return value;
            }
        }
    }
}
