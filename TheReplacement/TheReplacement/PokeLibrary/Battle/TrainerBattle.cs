﻿using TheReplacement.PokeLibrary.Battle.Field.Enums;
using TheReplacement.PokeLibrary.Pokemon;
using TheReplacement.PokeLibrary.Trainer;

namespace TheReplacement.PokeLibrary.Battle
{
    /// <summary>
    /// Defines a Pokemon battle between two trainers
    /// </summary>
    public class TrainerBattle : BattleBase<PlayerCharacter, TrainerPokemon>
    {
        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="style"></param>
        /// <param name="location"></param>
        /// <param name="terrain"></param>
        /// <param name="weather"></param>
        public TrainerBattle(BattleStyle style, Location location, Terrain terrain, Weather weather) : base(style, location, terrain, weather)
        {

        }
    }
}
