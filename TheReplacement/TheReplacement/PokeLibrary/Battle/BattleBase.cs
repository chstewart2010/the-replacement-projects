﻿using System;
using System.Collections.Generic;
using TheReplacement.PokeLibrary.Battle.Field;
using TheReplacement.PokeLibrary.Battle.Field.Enums;
using TheReplacement.PokeLibrary.Pokemon;
using TheReplacement.PokeLibrary.Trainer;

namespace TheReplacement.PokeLibrary.Battle
{
    /// <summary>
    /// Defines the base battle class
    /// </summary>
    /// <typeparam name="TTrainer">Type of Trainer</typeparam>
    /// <typeparam name="TPokemon">Type of Pokemon</typeparam>
    public class BattleBase<TTrainer, TPokemon> where TTrainer : TrainerBase<TPokemon> where TPokemon : PokemonBase
    {
        private bool _trickRoomActive = false;
        private int _trickRoomCounter = 0;
        private int _opponentCounter = 0;
        private readonly Random _rng = new Random();
        public List<string> participated = new List<string>();

        public FieldStruct Field { get; }
        public int TurnsPassed { get; private set; }
        /// <summary>
        /// Determine where a battle has ended
        /// </summary>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="style"></param>
        /// <param name="location"></param>
        /// <param name="terrain"></param>
        /// <param name="weather"></param>
        public BattleBase(BattleStyle style, Location location, Terrain terrain, Weather weather)
        {
            Field = new FieldStruct(style, location, terrain, weather);
            TurnsPassed = 0;
            IsDisposed = false;
        }

        /// <summary>
        /// Method to call when performing a turn in battle
        /// </summary>
        /// <param name="character"></param>
        /// <param name="move1"></param>
        /// <param name="opponent"></param>
        /// <param name="move2"></param>
        /// <param name="replacement"></param>
        public virtual void ExecuteTurn(ref PlayerCharacter character, string move1, ref TTrainer opponent, string move2, string replacement = null)
        {
            int i = _opponentCounter;
            //determine turn order
            bool characterGoesFirst = character.Team[0].BattleStats.Speed == opponent.Team[i].BattleStats.Speed
                ? _rng.Next(2) == 1
                : !_trickRoomActive && character.Team[0].BattleStats.Speed > opponent.Team[i].BattleStats.Speed;
            if (!participated.Contains(character.Team[0].Nickname))
            {
                participated.Add(character.Team[0].Nickname);
            }

            //perform actions before move declaration

            //swtich out
            bool switchOut = false;
            if (replacement != null)
            {
                switchOut = true;
                //handle pursuit mechanic
                character.SetActivePokemon(replacement);
                if (!participated.Contains(character.Team[0].Nickname))
                {
                    participated.Add(character.Team[0].Nickname);
                }
            }

            string current = character.Team[0].Nickname;

            //perform actions
            if (characterGoesFirst && !switchOut)
            {
                //check for feint and protect
                //check for helping hand
                //check for protect
                //check for guard and fakeout
                //check for p2 moves
                //check for p1 moves

                //normal phase

                //check for vital throw
                //check for beak blast focus punch shell trap
                //check for avalance revenge
                //check for counter mirror coat
                //check for non-damaging removal moves
                //check for trick room activation
            }
            else
            {
                //check for feint and protect
                //check for helping hand
                //check for protect
                //check for guard and fakeout
                //check for p2 moves
                //check for p1 moves

                //normal phase

                //check for vital throw
                //check for beak blast focus punch shell trap
                //check for avalance revenge
                //check for counter mirror coat
                //check for non-damaging removal moves
                //check for trick room activation
            }

            if (opponent.Team[i].BattleStats.CurrentHP == 0)
            {
                foreach (string participant in participated)
                {
                    character.SetActivePokemon(participant);
                    character.Team[0].GainExp(opponent.Team[i].GetExpYield(opponent.Team[i], true, true));
                }
                character.SetActivePokemon(current);
                participated = new List<string>();
                _opponentCounter++;
                if (_opponentCounter == opponent.Team.Count)
                {
                    EndBattle();
                }
            }
            Field.IncrementCountersDown();
            if (_trickRoomActive)
            {
                _trickRoomCounter--;
                _trickRoomActive = _trickRoomCounter == 0;
            }

            TurnsPassed++;
        }

        /// <summary>
        /// Sets IsDisposed to true
        /// </summary>
        public void EndBattle()
        {
            IsDisposed = true;
        }
    }
}
