﻿namespace TheReplacement.PokeLibrary.Battle.Enums.Battle
{
    /// <summary>
    /// Defines all of the possible levels of priority
    /// </summary>
    enum Priority
    {
        QuickClawActivation = 9,
        SwitchOutChargingOrItemUse = 8,
        MegaEvolutionAndDynamax = 7,
        PurSuitOnSwitchOut = 6,
        HelpingHand = 5,
        MainShielding = 4,
        FakeOutAndSecondaryShielding = 3,
        AllySwitchRagePowderAndPriority2Attacks = 2,
        Priority1Attacks = 1,
        None = 0,
        VitalThrow = -1,
        BeakBlastFocusPunchShellTrap = -3,
        AvalancheRevenge = -4,
        Counter = -5,
        Removal = -6,
        TrickRoom = -7
    }
}
