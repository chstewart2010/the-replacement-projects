﻿using TheReplacement.PokeLibrary.Pokeball.Enum;

namespace TheReplacement.PokeLibrary.Pokemon
{
    /// <summary>
    /// Defines the the Pokemon Egg class
    /// </summary>
    public class Egg : PokemonBase
    {
        /// <summary>
        /// Eggs from the mother
        /// </summary>
        public Pokeballs Pokeball { get; }

        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="species"></param>
        /// <param name="ball"></param>
        /// <param name="form"></param>
        /// <param name="ability"></param>
        /// <param name="shinyChance"></param>
        public Egg(string species, Pokeballs ball, string form, string ability, int shinyChance = 4096)
            : base(species: species, form: form, ability: ability, level: 1, heldItem: "", shinyChance: shinyChance)
        {
            Pokeball = ball;
        }
    }
}
