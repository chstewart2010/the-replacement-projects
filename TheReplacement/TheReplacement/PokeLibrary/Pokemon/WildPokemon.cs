﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using TheReplacement.PokeLibrary.Pokeball.Enum;
using TheReplacement.PokeLibrary.Pokemon.PokemonParts;

namespace TheReplacement.PokeLibrary.Pokemon
{
    /// <summary>
    /// Defines the class for Wild Pokemon
    /// </summary>
    public class WildPokemon : PokemonBase
    {
        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="species"></param>
        /// <param name="form"></param>
        /// <param name="ability"></param>
        /// <param name="level"></param>
        /// <param name="heldItem"></param>
        /// <param name="shinyChance"></param>
        public WildPokemon(string species, string form, string ability, uint level, string heldItem = "", int shinyChance = 4096)
            : base(species: species, form: form, ability: ability, level: level, heldItem: heldItem, shinyChance: shinyChance) { }

        internal static WildPokemon CreateFromObject(object ob)
        {
            var json = JObject.Parse(JsonConvert.SerializeObject(ob));
            return new WildPokemon(json.GetStringFromToken("Species"), json.GetStringFromToken("Form"),
                json.GetStringFromToken("Ability"), json.GetIntFromToken("Level"));
        }
        internal WildPokemon(string species, string form, string ability, string nature, uint level, string heldItem, IVs? ivs, EVs? evs, string[] moves, Guid id, bool isShiny, Gender gender)
           : base(species, form, ability, nature, level, 0, heldItem, ivs, evs, moves, id, isShiny: isShiny)
        {
            Gender = gender;
        }

        internal static WildPokemon LoadFromJSON(JToken pokemon)
        {
            WildPokemon temp = new WildPokemon(pokemon.GetStringFromToken("Species"),
                    pokemon.GetStringFromToken("Form"), pokemon.GetStringFromToken("Ability"),
                    pokemon.GetStringFromToken("Nature"), pokemon.GetIntFromToken("Level"),
                    pokemon.GetStringFromToken("HeldItem"), new IVs(pokemon["IVs"]), new EVs(pokemon["EVs"]),
                    GetMovesFromJSON(pokemon["Moves"]), Guid.Parse(pokemon.GetStringFromToken("TrainerID")),
                    pokemon.GetBoolFromToken("IsShiny"), (Gender)pokemon.GetIntFromToken("Gender"))
            {
                Nickname = pokemon.GetStringFromToken("Nickname")
            };
            temp.BattleStats = temp.PokemonStats;
            temp.BattleStats.SetCurrentStatus((Status)pokemon.GetIntFromToken("Status"));

            return temp;
        }

        private static string[] GetMovesFromJSON(JToken moves)
        {
            var temp = new string[4] { "", "", "", "" };
            JArray array = (JArray)moves;

            for (int i = 0; i < 4; i++)
            {
                temp[i] = array[i].GetStringFromToken();
            }

            return temp;
        }
    }
}
