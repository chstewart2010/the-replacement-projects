﻿using Newtonsoft.Json.Linq;
using System;

namespace TheReplacement.PokeLibrary.Pokemon.PokemonParts
{
    /// <summary>
    /// Defines the pokemons stats
    /// </summary>
    public class Stats : IStats
    {
        public uint Level { get; private set; }
        public uint ExperiencePoints { get; private set; }
        public ExperienceGrowthRate ExperienceGrowthRate { get; private set; }
        public uint ExpUntilNextLevel => (uint)ExperienceGrowthRate.GetExpNeeded(Level);
        public JToken Nature { get; }
        public BaseStats BaseStats { get; }
        public IVs IVs { get; }
        public EVs EVs { get; }
        public Stages CurrentStages { get; private set; }
        public Status Status { get; set; }
        public uint HP => BaseStats.HP == 1 ? 1 : SetHP(BaseStats.HP, IVs.HP, EVs.HP, Level);
        public uint CurrentHP { get; private set; }
        public uint Attack =>
            MakeStat(BaseStats.Attack, IVs.Attack, EVs.Attack, Level, "attack", Nature, CurrentStages.Attack);
        public uint Defense =>
            MakeStat(BaseStats.Defense, IVs.Defense, EVs.Defense, Level, "defense", Nature, CurrentStages.Defense);
        public uint SpecialAttack =>
            MakeStat(BaseStats.SpecialAttack, IVs.SpecialAttack, EVs.SpecialAttack, Level, "special=attack", Nature, CurrentStages.SpecialAttack);
        public uint SpecialDefense => 
            MakeStat(BaseStats.SpecialDefense, IVs.SpecialDefense, EVs.SpecialDefense, Level, "special-defense", Nature, CurrentStages.SpecialDefense);
        public uint Speed => Status == Status.Paralyzed
                ? MakeStat(BaseStats.Speed, IVs.Speed, EVs.Speed, Level, "speed", Nature, CurrentStages.Speed) / 2
                : MakeStat(BaseStats.Speed, IVs.Speed, EVs.Speed, Level, "speed", Nature, CurrentStages.Speed); 

        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="baseStats"></param>
        /// <param name="ivs"></param>
        /// <param name="evs"></param>
        /// <param name="level"></param>
        /// <param name="growthRate"></param>
        /// <param name="status"></param>
        /// <param name="nature"></param>
        public Stats(BaseStats baseStats, IVs ivs, EVs evs, string growthRate, Status status, JToken nature,
            uint level = 0, uint exp = 0)
        {
            ExperienceGrowthRate = SetRate(growthRate);
            ExperiencePoints = level == 0
                ? exp == 0
                    ? 0
                    : exp
                : (uint)ExperienceGrowthRate.GetExpNeeded(level - 1);
            Level = ExperiencePoints >= ExperienceGrowthRate.GetExpNeeded(level) && ExperiencePoints <= ExperienceGrowthRate.GetExpNeeded(level + 1)
                ? level
                : SetLevel();
            Nature = nature;
            Status = status;
            IVs = ivs;
            EVs = evs;
            BaseStats = baseStats;
            CurrentStages = Stages.MakeNewStages();
            CurrentHP = SetHP(BaseStats.HP, IVs.HP, EVs.HP, Level);
        }

        /// <summary>
        /// Sets the current status
        /// </summary>
        /// <param name="status"></param>
        public void SetCurrentStatus(Status status)
        {
            if (status == Status.Normal)
            {
                if (Status != Status.Normal)
                {
                    Status = status;
                }
            }
            else
            {
                if (Status == Status.Normal)
                {
                    Status = status;
                }
            }
        }

        /// <summary>
        /// Sets the Current HP stat
        /// </summary>
        /// <param name="hp"></param>
        public void SetCurrentHP(uint hp)
        {
            CurrentHP = hp < 0
                ? 0
                : hp > HP
                    ? HP
                    : hp;
        }

        /// <summary>
        /// Increase EXP
        /// </summary>
        /// <param name="expGain"></param>
        /// <param name="yields"></param>
        public bool GainExp(uint expGain, EffortYields? yields = null)
        {
            if (yields != null)
            {
                var temp = yields.Value;
                EVs.AddEVs(temp.HP, "hp");
                EVs.AddEVs(temp.Attack, "attack");
                EVs.AddEVs(temp.Defense, "defense");
                EVs.AddEVs(temp.SpecialAttack, "special-attack");
                EVs.AddEVs(temp.SpecialDefense, "special-defense");
                EVs.AddEVs(temp.Speed, "speed");
            }
            if (Level == 100)
                return false;
            bool levelUP = false;
            ExperiencePoints += expGain;
            uint placeholder = 0;
            while (ExperiencePoints >= ExpUntilNextLevel)
            {
                if (Level == 100)
                {
                    ExperiencePoints = placeholder;
                    return levelUP;
                }
                Level++;
                if (!levelUP) levelUP = true;
                placeholder = ExpUntilNextLevel;
            }

            return levelUP;
        }

        private uint SetLevel()
        {
            uint tempLevel = 1;
            uint placeholder = (uint)ExperienceGrowthRate.GetExpNeeded(tempLevel);
            while (ExperiencePoints >= placeholder)
            {
                if (tempLevel == 100)
                {
                    ExperiencePoints = placeholder;
                    return tempLevel;
                }
                tempLevel++;
                placeholder = (uint)ExperienceGrowthRate.GetExpNeeded(tempLevel);
            }

            return tempLevel;
        }

        /// <summary>
        /// Import stages and HP from a different pokemon
        /// </summary>
        /// <param name="stages"></param>
        /// <param name="hp"></param>
        internal void ImportPrevious(Stages stages, uint hp)
        {
            CurrentStages = stages;
            CurrentHP = hp;
        }

        /// <summary>
        /// Reset stat stages
        /// </summary>
        public void ResetStats()
        {
            CurrentStages.ResetStages();
        }

        /// <summary>
        /// Increments sta
        /// </summary>
        /// <param name="increase">true for up, false, for down</param>
        /// <param name="number">number of stages</param>
        /// <param name="stat">Case-insensitive, hypenated form </param>
        public void IncrementStat(bool increase, int number, string stat)
        {
            stat = stat.ToLower().Replace(" ", "-");
            bool success = false;
            for (int i = 0; i < number; i++)
            {
                bool temp = increase
                ? CurrentStages.IncrementUp(stat)
                : CurrentStages.IncrementDown(stat);

                success = success || temp;
            }
        }

        private static ExperienceGrowthRate SetRate(string rate)
        {
            switch (rate)
            {
                case "slow-then-very-fast":
                    return ExperienceGrowthRate.Erratic;
                case "fast":
                    return ExperienceGrowthRate.Fast;
                case "medium":
                    return ExperienceGrowthRate.MediumFast;
                case "medium-slow":
                    return ExperienceGrowthRate.MediumSlow;
                case "slow":
                    return ExperienceGrowthRate.Slow;
                default:
                    return ExperienceGrowthRate.Fluctuating;
            }
        }

        private static uint SetHP(double hpBase, double hpIVs, double hpEVs, double level)
        {
            return (uint)(((2 * hpBase + hpIVs + Math.Floor(hpEVs / 4)) * level / 100) + level + 10);
        }

        private static uint MakeStat(uint baseStat, uint ivs, uint evs, uint level, string stat, JToken nature, int stage)
        {
            return stage > 0
                ? (uint)(Math.Round((Math.Round((2 * baseStat + ivs + Math.Round((double)(evs / 4), 0)) * level / 100, 0) + 5) * (10 + IncreaseByNature(stat, nature) + DecreaseByNature(stat, nature)) / 10, 0) * (2 + stage) / 2)
                : (uint)(Math.Round((Math.Round((2 * baseStat + ivs + Math.Round((double)(evs / 4), 0)) * level / 100, 0) + 5) * (10 + IncreaseByNature(stat, nature) + DecreaseByNature(stat, nature)) / 10, 0) * 2 / (2 - stage));
        }

        private static int IncreaseByNature(string stat, JToken nature)
        {
            var token = nature["increased_stat"];
            return string.IsNullOrEmpty(token.ToString())
                ? 0
                : (string)token["name"] == stat
                    ? 1
                    : 0;
        }

        private static int DecreaseByNature(string stat, JToken nature)
        {
            var token = nature["decreased_stat"];
            return string.IsNullOrEmpty(token.ToString())
                ? 0
                : (string)token["name"] == stat
                    ? -1
                    : 0;
        }
    }

    /// <summary>
    /// Defines the pokemons base stats structures
    /// </summary>
    public struct BaseStats : IStats
    {
        public uint HP { get; }
        public uint Attack { get; }
        public uint Defense { get; }
        public uint SpecialAttack { get; }
        public uint SpecialDefense { get; }
        public uint Speed { get; }

        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="hp"></param>
        /// <param name="attack"></param>
        /// <param name="defense"></param>
        /// <param name="sAttack"></param>
        /// <param name="sDefense"></param>
        /// <param name="speed"></param>
        public BaseStats(uint hp, uint attack, uint defense, uint sAttack, uint sDefense, uint speed)
        {
            HP = hp;
            Attack = attack;
            Defense = defense;
            SpecialAttack = sAttack;
            SpecialDefense = sDefense;
            Speed = speed;
        }
    }

    /// <summary>
    /// Defines the pokemons IVs
    /// </summary>
    public struct IVs : IStats
    {
        private static Random _rng;

        public uint HP { get; }
        public uint Attack { get; }
        public uint Defense { get; }
        public uint SpecialAttack { get; }
        public uint SpecialDefense { get; }
        public uint Speed { get; }

        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="hp"></param>
        /// <param name="attack"></param>
        /// <param name="defense"></param>
        /// <param name="sAttack"></param>
        /// <param name="sDefense"></param>
        /// <param name="speed"></param>
        public IVs(uint hp, uint attack, uint defense, uint sAttack, uint sDefense, uint speed)
        {
            _rng = new Random();
            HP = IntegrityCheck(hp);
            Attack = IntegrityCheck(attack);
            Defense = IntegrityCheck(defense);
            SpecialAttack = IntegrityCheck(sAttack);
            SpecialDefense = IntegrityCheck(sDefense);
            Speed = IntegrityCheck(speed);
        }

        internal IVs(JToken token)
        {
            _rng = new Random();
            HP = token.GetIntFromToken("HP");
            Attack = token.GetIntFromToken("Attack");
            Defense = token.GetIntFromToken("Defense");
            SpecialAttack = token.GetIntFromToken("SpecialAttack");
            SpecialDefense = token.GetIntFromToken("SpecialDefense");
            Speed = token.GetIntFromToken("Speed");
        }

        internal object ToObject()
        {
            return new
            {
                HP,
                Attack,
                Defense,
                SpecialAttack,
                SpecialDefense,
                Speed
            };
        }

        private static uint IntegrityCheck(uint iv)
        {
            return iv > 31
                ? (uint)_rng.Next(32)
                : iv;
        }
    }

    /// <summary>
    /// Defines the pokemons EVs
    /// </summary>
    public struct EVs : IStats
    {
        private static Random _rng;
        private const int EVMAX = 508;
        private uint _evTotal;

        public uint HP { get; private set; }
        public uint Attack { get; private set; }
        public uint Defense { get; private set; }
        public uint SpecialAttack { get; private set; }
        public uint SpecialDefense { get; private set; }
        public uint Speed { get; private set; }

        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="hp"></param>
        /// <param name="attack"></param>
        /// <param name="defense"></param>
        /// <param name="sAttack"></param>
        /// <param name="sDefense"></param>
        /// <param name="speed"></param>
        public EVs(uint hp, uint attack, uint defense, uint sAttack, uint sDefense, uint speed)
        {
            _rng = new Random();
            _evTotal = 0;
            HP = IntegrityCheck(hp, ref _evTotal);
            Attack = IntegrityCheck(attack, ref _evTotal);
            Defense = IntegrityCheck(defense, ref _evTotal);
            SpecialAttack = IntegrityCheck(sAttack, ref _evTotal);
            SpecialDefense = IntegrityCheck(sDefense, ref _evTotal);
            Speed = IntegrityCheck(speed, ref _evTotal);
        }

        internal EVs(JToken token)
        {
            _rng = new Random();
            HP = token.GetIntFromToken("HP");
            Attack = token.GetIntFromToken("Attack");
            Defense = token.GetIntFromToken("Defense");
            SpecialAttack = token.GetIntFromToken("SpecialAttack");
            SpecialDefense = token.GetIntFromToken("SpecialDefense");
            Speed = token.GetIntFromToken("Speed");
            _evTotal = HP + Attack + Defense + SpecialAttack + SpecialDefense + Speed;
        }

        internal object ToObject()
        {
            return new
            {
                HP,
                Attack,
                Defense,
                SpecialAttack,
                SpecialDefense,
                Speed
            };
        }

        /// <summary>
        /// Add EVs to total
        /// </summary>
        /// <param name="hp"></param>
        /// <param name="attack"></param>
        /// <param name="defense"></param>
        /// <param name="sAttack"></param>
        /// <param name="sDefense"></param>
        /// <param name="speed"></param>
        public void AddEVs(uint evs, string stat)
        {
            switch (stat)
            {
                case "hp":
                    if (HP + evs <= 252 && evs + _evTotal <= EVMAX)
                    {
                        HP += evs;
                        _evTotal += evs;
                    }
                    break;
                case "attack":
                    if (Attack + evs <= 252 && evs + _evTotal <= EVMAX)
                    {
                        Attack += evs;
                        _evTotal += evs;
                    }
                    break;
                case "defense":
                    if (Defense + evs <= 252 && evs + _evTotal <= EVMAX)
                    {
                        Defense += evs;
                        _evTotal += evs;
                    }
                    break;
                case "special-attack":
                    if (SpecialAttack + evs <= 252 && evs + _evTotal <= EVMAX)
                    {
                        SpecialAttack += evs;
                        _evTotal += evs;
                    }
                    break;
                case "special-defense":
                    if (SpecialDefense + evs <= 252 && evs + _evTotal <= EVMAX)
                    {
                        SpecialDefense += evs;
                        _evTotal += evs;
                    }
                    break;
                case "speed":
                    if (Speed + evs <= 252 && evs + _evTotal <= EVMAX)
                    {
                        Speed += evs;
                        _evTotal += evs;
                    }
                    break;
            }
        }

        private static uint IntegrityCheck(uint ev, ref uint evTotal)
        {
            if (evTotal != EVMAX)
            {
                if (ev < 0 || ev > 252)
                {
                    ev = (uint)_rng.Next(253);
                }
                if (ev + evTotal > EVMAX)
                {
                    uint difference = ev + evTotal - EVMAX;
                    ev -= difference;
                }

                evTotal += ev;
                return ev;
            }
            else
            {
                return 0;
            }
        }
    }

    /// <summary>
    /// Defines the pokemons stat stages
    /// </summary>
    public struct Stages
    {
        public int HP { get; private set; }
        public int Attack { get; private set; }
        public int Defense { get; private set; }
        public int SpecialAttack { get; private set; }
        public int SpecialDefense { get; private set; }
        public int Speed { get; private set; }

        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="hp"></param>
        /// <param name="attack"></param>
        /// <param name="defense"></param>
        /// <param name="sAttack"></param>
        /// <param name="sDefense"></param>
        /// <param name="speed"></param>
        public static Stages MakeNewStages()
        {
            return new Stages()
            {
                HP = 0,
                Attack = 0,
                Defense = 0,
                SpecialAttack = 0,
                SpecialDefense = 0,
                Speed = 0
            };
        }

        internal void ResetStages()
        {
            Attack = 0;
            Defense = 0;
            SpecialAttack = 0;
            SpecialDefense = 0;
            Speed = 0;
        }

        internal bool IncrementUp(string stat)
        {
            switch (stat.ToLower().Replace(" ", "-"))
            {
                case "attack":
                    if (Attack < 6)
                    {
                        Attack++;
                        return true;
                    }
                    break;
                case "defense":
                    if (Defense < 6)
                    {
                        Defense++;
                        return true;
                    }
                    break;
                case "special-attack":
                    if (SpecialAttack < 6)
                    {
                        SpecialAttack++;
                        return true;
                    }
                    break;
                case "special-defense":
                    if (SpecialDefense < 6)
                    {
                        SpecialDefense++;
                        return true;
                    }
                    break;
                case "speed":
                    if (Speed < 6)
                    {
                        Speed++;
                        return true;
                    }
                    break;
                default:
                    break;
            }

            return false;
        }

        internal bool IncrementDown(string stat)
        {
            switch (stat.ToLower().Replace(" ", "-"))
            {
                case "attack":
                    if (Attack > -6)
                    {
                        Attack--;
                        return true;
                    }
                    break;
                case "defense":
                    if (Defense > -6)
                    {
                        Defense--;
                        return true;
                    }
                    break;
                case "special-attack":
                    if (SpecialAttack > -6)
                    {
                        SpecialAttack--;
                        return true;
                    }
                    break;
                case "special-defense":
                    if (SpecialDefense > -6)
                    {
                        SpecialDefense--;
                        return true;
                    }
                    break;
                case "speed":
                    if (Speed > -6)
                    {
                        Speed--;
                        return true;
                    }
                    break;
                default:
                    break;
            }

            return false;
        }
    }

    /// <summary>
    /// Defines the pokemons Effort Yields
    /// </summary>
    public struct EffortYields : IStats
    {
        public uint HP { get; }
        public uint Attack { get; }
        public uint Defense { get; }
        public uint SpecialAttack { get; }
        public uint SpecialDefense { get; }
        public uint Speed { get; }

        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="hp"></param>
        /// <param name="attack"></param>
        /// <param name="defense"></param>
        /// <param name="sAttack"></param>
        /// <param name="sDefense"></param>
        /// <param name="speed"></param>
        public EffortYields(uint hp, uint attack, uint defense, uint sAttack, uint sDefense, uint speed)
        {
            HP = hp;
            Attack = attack;
            Defense = defense;
            SpecialAttack = sAttack;
            SpecialDefense = sDefense;
            Speed = speed;
        }
    }

    /// <summary>
    /// Defines the items for types of stats
    /// </summary>
    interface IStats
    {
        uint HP { get; }
        uint Attack { get; }
        uint Defense { get; }
        uint SpecialAttack { get; }
        uint SpecialDefense { get; }
        uint Speed { get; }
    }
}