﻿using System;

namespace TheReplacement.PokeLibrary.Pokemon.PokemonParts
{
    /// <summary>
    /// Gender container
    /// </summary>
    public enum Gender
    {
        /// <summary>
        /// Neuter
        /// </summary>
        Neuter,

        /// <summary>
        /// Male
        /// </summary>
        Male,

        /// <summary>
        /// Female
        /// </summary>
        Female
    }

    /// <summary>
    /// Status container
    /// </summary>
    public enum Status
    {
        /// <summary>
        /// Fainted status. Pokemon cannot battle. Pokemon cannot be caught
        /// </summary>
        [StatusModifier(0)]
        Fainted,

        /// <summary>
        /// No status.
        /// </summary>
        [StatusModifier(1)]
        Normal,

        /// <summary>
        /// Burned status. Catch rate increased by 50%. Attack power on physical moves reduced by 50%.
        /// <para/>Pokemon loses 1/16th of its max HP after each turn. Fire pokemon are immuned.
        /// </summary>
        [StatusModifier(1.5)]
        Burned,

        /// <summary>
        /// Paralyzed status. Catch rate increased by 50%. Speed reduced by 50%.
        /// <para/>50% chance to be unable to move. Electric pokemon are immuned.
        /// </summary>
        [StatusModifier(1.5)]
        Paralyzed,

        /// <summary>
        /// Poisoned status. Catch rate increased by 50%.
        /// <para/>Pokemon loses 1/8th of its max HP after each turn.
        /// <para/>Poison pokemon are immuned, unless trigger by Corrosion ability.
        /// </summary>
        [StatusModifier(1.5)]
        Poisoned,

        /// <summary>
        /// Badly Poisoned status. Catch rate increased by 50%.
        /// <para/>Pokemon loses n/16th of its max HP after its turn, where n is the number of turns poisoned.
        /// <para/>Poison pokemon are immuned, unless trigger by Corrosion ability.
        /// </summary>
        [StatusModifier(1.5)]
        Toxic,

        /// <summary>
        /// Frozen status. Catch rate increased by 150%. Pokemon is unable to move.
        /// <para/> Ice pokemon are immuned.
        /// </summary>
        [StatusModifier(2.5)]
        Frozen,

        /// <summary>
        /// Sleep status. Catch rate increased by 150%. Pokemon is unable to move.
        /// </summary>
        [StatusModifier(2.5)]
        Asleep
    }

    /// <summary>
    /// Container for possible typings
    /// </summary>
    [Flags]
    public enum Types
    {
        [Types(new Types[] { Fire, Grass, Bug, Ice, Steel, Fairy }, new Types[] { Water, Rock, Ground }, new Types[] { })]
        Fire = 1 << 0,

        [Types(new Types[] { Fire, Water, Ice, Steel }, new Types[] { Grass, Electric }, new Types[] { })]
        Water = 1 << 1,

        [Types(new Types[] { Water, Grass, Ground }, new Types[] { Fire, Bug, Flying, Ice }, new Types[] { })]
        Grass = 1 << 2,

        [Types(new Types[] { Flying, Steel }, new Types[] { Ground }, new Types[] { })]
        Electric = 1 << 3,

        [Types(new Types[] { Grass, Ground, Fighting }, new Types[] { Fire, Flying, Rock }, new Types[] { })]
        Bug = 1 << 4,

        [Types(new Types[] { }, new Types[] { Fighting }, new Types[] { Ghost })]
        Normal = 1 << 5,

        [Types(new Types[] { Grass, Bug, Fighting }, new Types[] { Electric, Rock, Ice }, new Types[] { Ground })]
        Flying =  1 << 6,

        [Types(new Types[] { Poison, Rock }, new Types[] { Water, Grass, Ice }, new Types[] { Electric })]
        Ground = 1 << 7,

        [Types(new Types[] { Grass, Bug, Fighting, Fairy }, new Types[] { Ground, Psychic }, new Types[] { })]
        Poison = 1 << 8,

        [Types(new Types[] { Fire, Flying, Poison }, new Types[] { Water, Grass, Ground, Fighting }, new Types[] { })]
        Rock = 1 << 9,

        [Types(new Types[] { Bug, Rock, Dark }, new Types[] { Flying, Psychic, Fairy }, new Types[] { })]
        Fighting = 1 << 10,

        [Types(new Types[] { Fighting, Psychic }, new Types[] { Bug, Ghost, Dark }, new Types[] { })]
        Psychic = 1 << 11,

        [Types(new Types[] { Bug, Poison }, new Types[] { Ghost, Dark }, new Types[] { Normal, Fighting })]
        Ghost = 1 << 12,

        [Types(new Types[] { Ice }, new Types[] { Fire, Rock, Fighting, Steel }, new Types[] { })]
        Ice = 1 << 13,

        [Types(new Types[] { Fire, Water, Grass, Electric }, new Types[] { Ice, Dragon, Fairy }, new Types[] { })]
        Dragon = 1 << 14,

        [Types(new Types[] { Ghost, Dark }, new Types[] { Bug, Fighting, Fairy }, new Types[] { Psychic })]
        Dark = 1 << 15,

        [Types(new Types[] { Grass, Bug, Normal, Flying, Psychic, Ice, Dragon, Steel, Fairy, Rock }, new Types[] { Fire, Ground, Fighting }, new Types[] { Poison })]
        Steel = 1 << 16,

        [Types(new Types[] { Bug, Fighting, Dark }, new Types[] { Poison, Steel }, new Types[] { Dragon })]
        Fairy = 1 << 17
    }

    /// <summary>
    /// Defines the container for possible EXP growth rates
    /// </summary>
    public enum ExperienceGrowthRate
    {
        Erratic,
        Fast,
        MediumFast,
        MediumSlow,
        Slow,
        Fluctuating
    }
}
