﻿using System;

namespace TheReplacement.PokeLibrary.Pokemon.PokemonParts
{

    /// <summary>
    /// Container for the Status Modifier
    /// </summary>
    internal class StatusModifierAttribute : Attribute
    {
        /// <summary>
        /// Pokeball Status Modifier value
        /// </summary>
        public double StatusMultiplier { get; }

        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="modifier">status modifier value</param>
        public StatusModifierAttribute(double modifier)
        {
            StatusMultiplier = modifier;
        }
    }

    /// <summary>
    /// Container for Type Effectiveness
    /// </summary>
    public class TypesAttribute : Attribute
    {
        /// <summary>
        /// Collection of resisted Types
        /// </summary>
        public Types[] Resistances { get; }

        /// <summary>
        /// Collection of super-effective Types
        /// </summary>
        public Types[] Weaknesses { get; }

        /// <summary>
        /// Collection of Immunities
        /// </summary>
        public Types[] Immunities { get; }

        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="resistances">Types that are resisted</param>
        /// <param name="weaknesses"> Types that are super effective</param>
        /// <param name="immunities"> Types that are ineffective</param>
        public TypesAttribute(Types[] resistances, Types[] weaknesses, Types[] immunities)
        {
            Resistances = resistances;
            Weaknesses = weaknesses;
            Immunities = immunities;
        }
    }
}
