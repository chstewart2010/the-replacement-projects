﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace TheReplacement.PokeLibrary.Pokemon.PokemonCollection
{
    /// <summary>
    /// Defines the base class for Pokemon Collections
    /// </summary>
    /// <typeparam name="T">Any Type</typeparam>
    public class PokemonCollectionBase<T> : IList<T>
    {
        private readonly IList<T> _pokemonCollection;
        public int TotalCount { get; }

        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="totalCount"></param>
        public PokemonCollectionBase(int totalCount)
        {
            _pokemonCollection = new List<T>();
            TotalCount = totalCount;
        }

        public T this[int index]
        {
            get
            {
                return _pokemonCollection[index];
            }

            set
            {
                if (index > TotalCount)
                {
                    throw new IndexOutOfRangeException();
                }
                else
                {
                    _pokemonCollection[index] = value;
                }
            }
        }

        /// <summary>
        /// Count in collection
        /// </summary>
        public int Count => _pokemonCollection.Count;

        /// <summary>
        /// Always false
        /// </summary>
        public bool IsReadOnly => false;

        /// <summary>
        /// Add a pokemon to the collection
        /// </summary>
        /// <param name="item"></param>
        public void Add(T item)
        {
            if (Count == TotalCount)
            {
                throw new IndexOutOfRangeException();
            }
            else
            {
                _pokemonCollection.Add(item);
            }
        }

        /// <summary>
        /// Removes all items from the collection
        /// </summary>
        public void Clear()
        {
            _pokemonCollection.Clear();
        }

        /// <summary>
        /// Returns true if the collection contains the item
        /// </summary>
        /// <param name="item"></param>
        public bool Contains(T item)
        {
            return _pokemonCollection.Contains(item);
        }

        /// <summary>
        /// Copys pokemon to an Array
        /// </summary>
        /// <param name="array"></param>
        /// <param name="arrayIndex"></param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            _pokemonCollection.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Gets an enumerator
        /// </summary>
        /// <returns></returns>
        public IEnumerator<T> GetEnumerator()
        {
            return _pokemonCollection.GetEnumerator();
        }

        /// <summary>
        /// Gets the index of the pokemon
        /// </summary>
        /// <param name="item"></param>
        public int IndexOf(T item)
        {
            return _pokemonCollection.IndexOf(item);
        }

        /// <summary>
        /// Inserts an item at the given index
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        public void Insert(int index, T item)
        {
            if (index >= TotalCount)
            {
                throw new ArgumentOutOfRangeException();
            }
            else
            {
                _pokemonCollection.Insert(index, item);
            }
        }

        /// <summary>
        /// Returns true if the item is removed from the collection
        /// </summary>
        /// <param name="item"></param>
        public bool Remove(T item)
        {
            return _pokemonCollection.Remove(item);
        }

        /// <summary>
        /// Removes an item at the given index
        /// </summary>
        /// <param name="index"></param>
        public void RemoveAt(int index)
        {
            if (index >= TotalCount)
            {
                throw new ArgumentOutOfRangeException();
            }
            else
            {
                _pokemonCollection.RemoveAt(index);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }


    }
}
