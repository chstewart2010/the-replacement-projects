﻿namespace TheReplacement.PokeLibrary.Pokemon.PokemonCollection
{
    /// <summary>
    /// Defines the class for a (up to) 6 pokemon team
    /// </summary>
    /// <typeparam name="T">Wild or Trainer Pokemon</typeparam>
    public class TrainerTeam<T> : PokemonCollectionBase<T> where T : PokemonBase
    {
        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="pokemon"></param>
        public TrainerTeam(T[] pokemon) : base(6)
        {
            foreach (var poke in pokemon)
            {
                Add(poke);
            }
        }

        internal TrainerTeam() : base(6)
        {

        }

        public object[] ToObject()
        {
            object[] temp = new object[Count];
            for (int i = 0; i < Count; i++)
            {
                temp[i] = this[i].ToObject();
            }

            return temp;
        }

        /// <summary>
        /// Returns an anonymous object consisting of the Pokemon info from the Team.
        /// </summary>
        /// <param name="inBattle"></param>
        public object[] GetTeamInfo(bool inBattle)
        {
            object[] result = new object[Count];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = this[i].GetPokemonInfo(inBattle);
            }

            return result;
        }
    }

    /// <summary>
    /// Defines the class for a (up to) 30 pokemon box
    /// </summary>
    public class PokemonBox : PokemonCollectionBase<TrainerPokemon>
    {
        /// <summary>
        /// Constructor method
        /// </summary>
        public PokemonBox() : base(30)
        {

        }

        public object[] ToObject()
        {
            object[] temp = new object[Count];
            for (int i = 0; i < Count; i++)
            {
                temp[i] = this[i].ToObject();
            }

            return temp;
        }
    }


    /// <summary>
    /// Defines the class for a (up to) 30 pokemon bank
    /// </summary>
    public class PokemonBank : PokemonCollectionBase<PokemonBox>
    {
        public PokemonBank() : base(30)
        {
            Add(new PokemonBox());
        }

        public object[] ToObject()
        {
            object[] temp = new object[Count];
            for (int i = 0; i < Count; i++)
            {
                temp[i] = this[i].ToObject();
            }

            return temp;
        }
    }
}
