﻿using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.Linq;
using TheReplacement.PokeLibrary.Pokemon.PokemonParts;

namespace TheReplacement.PokeLibrary.Pokemon
{
    /// <summary>
    /// Base class of Pokemon creation
    /// </summary>
    public class PokemonBase
    {
        private protected readonly JObject _species;
        private protected readonly JObject _form;
        private protected readonly JObject _ability;
        private protected readonly JObject _nature;
        private protected readonly JObject _item;
        private protected readonly Random _rng;
        private protected readonly bool _isShiny;
        private protected readonly Types _typing;
        private protected readonly string _typeString;
        private protected readonly IVs _ivs;
        private protected readonly EVs _evs;
        private protected readonly BaseStats _baseStats;
        private protected readonly EffortYields _effortYields;
        private protected readonly int _baseExp;
        private protected Stats _stats;
        private string _getImageURL;

        /// <summary>
        /// Determines if a pokemon failed to construct
        /// </summary>
        public bool IsBugged { get; }
        public bool IsHoldingItem => _item == null;
        public JObject HeldItem => _item;
        public Types Types => _typing;
        public string EvolutionChain => (string)_species["evolution_chain"]["url"];
        public string Species => ((string)_species["name"]).Capitalize();
        public uint DexNo => (uint)_species["id"];
        public string Ability => ((string)_ability["name"]).Replace("-", " ").Capitalize();
        internal string Form => (string)_form["name"];
        public bool IsShiny => _isShiny;
        public Stats PokemonStats => _stats;
        public EffortYields EffortYields => _effortYields;
        public Gender Gender { get; protected internal set; }
        public int CaptureRate => (int)_species["capture_rate"];
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public string ImageURL => string.IsNullOrEmpty(_getImageURL)
                ? "https://cdn.discordapp.com/attachments/415030526429233162/704510934135341076/The_GuyTM.PNG"
                : _getImageURL;


        public JObject[] Moves { get; private set; }
        public Stats BattleStats { get; private protected set; }
        public string Nickname { get; set; }
        public Guid TrainerID { get; protected internal set; }

        protected PokemonBase(string species, string form = "", string ability = "", string nature="", uint level = 0, uint exp = 0, string heldItem = "", IVs? ivs = null, EVs? evs = null, string[] moves = null, Guid? id = null, int shinyChance = 4096, bool isShiny = false)
        {
            //set pokemon species
            _species = PokeAPI.GetSpecies(species);
            _rng = new Random();

            //terminate creation if error
            IsBugged = false;
            if (_species == null || string.IsNullOrEmpty(species))
            {
                IsBugged = true;
                return;
            }

            //set pokemon form
            string url = form.ToLower() == "random"
                ? (string)_species["varieties"][_rng.Next(((JArray)_species["varieties"]).Count)]["pokemon"]["url"]
                : GetURL(_species["varieties"], "pokemon", $"{form}");
            _form = string.IsNullOrEmpty(form)
                ? PokeAPI.GetResponse((string)_species["varieties"][0]["pokemon"]["url"])
                : string.IsNullOrEmpty(url)
                    ? PokeAPI.GetResponse((string)_species["varieties"][0]["pokemon"]["url"])
                    : PokeAPI.GetResponse(url);

            //set pokemon ability
            url = ability.ToLower() == "random"
                ? (string)_form["abilities"][_rng.Next(((JArray)_form["abilities"]).Count)]["ability"]["url"]
                : GetURL(_form["abilities"], "ability", ability.ToLower().Replace(" ", "-"));
            _ability = string.IsNullOrEmpty(ability)
                ? PokeAPI.GetResponse((string)_form["abilities"][_form["abilities"].Count() > 1 ? 1 : 0]["ability"]["url"])
                : string.IsNullOrEmpty(url)
                    ? PokeAPI.GetResponse((string)_form["abilities"][_form["abilities"].Count() > 1 ? 1 : 0]["ability"]["url"])
                    : PokeAPI.GetResponse(url);

            //set nickname
            Nickname = ((string)_species["name"]).Capitalize();

            //set Trainer ID
            TrainerID = id.GetValueOrDefault();

            //set typing
            var types = _form["types"];
            var typing = new Types[types.Children().Count()];
            _typing = default;
            for (int i = 0; i < types.Children().Count(); i++)
            {
                _typing |= (Types)Enum.Parse(typeof(Types), ((string)types[i]["type"]["name"]).Capitalize());
            }

            _typeString = string.Empty;
            foreach (Types value in Enum.GetValues(typeof(Types)))
            {
                if (_typing.HasFlag(value))
                {
                    _typeString += $"{value}/";
                }
            }

            // base exp yield
            _baseExp = (int)_form["base_experience"];

            //set ivs
            _ivs = ivs == null
                ? new IVs((uint)_rng.Next(32), (uint)_rng.Next(32), (uint)_rng.Next(32),
                (uint)_rng.Next(32), (uint)_rng.Next(32), (uint)_rng.Next(32))
                : ivs.GetValueOrDefault();

            //set evs
            _evs = evs == null
                ? _evs = new EVs(0, 0, 0, 0, 0, 0)
                : evs.GetValueOrDefault();

            //set base stat and ev yields
            var baseStats = _form["stats"];
            _baseStats = new BaseStats(
                (uint)baseStats[0]["base_stat"],
                (uint)baseStats[1]["base_stat"],
                (uint)baseStats[2]["base_stat"],
                (uint)baseStats[3]["base_stat"],
                (uint)baseStats[4]["base_stat"],
                (uint)baseStats[5]["base_stat"]
                );
            _effortYields = new EffortYields(
                (uint)baseStats[0]["effort"],
                (uint)baseStats[1]["effort"],
                (uint)baseStats[2]["effort"],
                (uint)baseStats[3]["effort"],
                (uint)baseStats[4]["effort"],
                (uint)baseStats[5]["effort"]
                );

            //set nature
            _nature = string.IsNullOrEmpty(nature)
                ? PokeAPI.GetNature(_rng.Next(1, 25).ToString())
                : PokeAPI.GetNature(nature.ToLower());

            //set stats
            string growthRate = (string)_species["growth_rate"]["name"];
            _stats = level < 1 || level > 100
                ? new Stats(_baseStats, _ivs, _evs, growthRate, Status.Normal, _nature, (uint)_rng.Next(1,101), exp)
                : new Stats(_baseStats, _ivs, _evs, growthRate, Status.Normal, _nature, level, exp);

            //set current stats
            BattleStats = _stats;

            //set shininess
            _isShiny = isShiny || _rng.Next(shinyChance) == 0;

            //set image
            //_getImageURL = _isShiny
            //        ? (string)_form["sprites"]["front_shiny"]
            //        : (string)_form["sprites"]["front_default"];

            string showdownPath = "https://play.pokemonshowdown.com/sprites";

            _getImageURL = isShiny
                ? $"{showdownPath}/ani-shiny/{Species.ToLower()}.gif"
                : $"{showdownPath}/ani/{Species.ToLower()}.gif";

            //set gender
            int genderRate = (int)_species["gender_rate"];
            Gender = genderRate == -1
                ? Gender.Neuter
                : genderRate == 0
                    ? Gender.Male
                    : _rng.Next(8) > genderRate
                        ? Gender.Male
                        : Gender.Female;

            //set held item
            _item = string.IsNullOrEmpty(heldItem)
                ? null
                : PokeAPI.GetItem(heldItem);

            //set moves
            var moveNames = moves ?? new string[4] { "", "", "", "" };
            int incrementor = 0;
            if (moves == null)
            {
                foreach (var move in _form["moves"])
                {
                    string name = (string)move["move"]["name"];
                    if (moveNames.Contains(name))
                    {
                        continue;
                    }
                    foreach (var detail in move["version_group_details"])
                    {
                        if (_stats.Level >= (int)detail["level_learned_at"] && (string)detail["move_learn_method"]["name"] == "level-up")
                        {
                            moveNames[incrementor % 4] = name;
                            incrementor++;
                            break;
                        }
                    }
                }
            }
            Moves = new JObject[4];
            for (int i = 0; i < 4; i++)
            {
                Moves[i] = string.IsNullOrEmpty(moveNames[i])
                    ? null
                    : PokeAPI.GetMove(moveNames[i]);
            }
        }

        /// <summary>
        /// Returns an object describing the pokemon
        /// </summary>
        /// <param name="inBattle"></param>
        public object GetPokemonInfo(bool inBattle)
        {
            return IsBugged
                ? null
                : new
                {
                    BattleStats.Level,
                    Species,
                    Nickname,
                    TrainerID,
                    Type = _typeString,
                    Ability,
                    Status = inBattle
                        ? BattleStats.Status.ToString()
                        : _stats.Status.ToString(),
                    Nature = ((string)_nature["name"]).Capitalize(),
                    HeldItem = _item == null
                        ? ""
                        : ((string)_item["name"]).Replace("-", " ").Capitalize(),
                    Experience = $"{_stats.ExperiencePoints}/{_stats.ExpUntilNextLevel}",
                    Stats = inBattle ? GetBattleStats() : GetPokemonStats(),
                    Shiny = _isShiny,
                    Gender = Gender.ToString(),
                    MoveList = new[]
                    {
                        Moves[0] == null
                            ? ""
                            :((string)Moves[0]["name"]).Replace("-", " ").Capitalize(),
                        Moves[1] == null
                            ? ""
                            : ((string)Moves[1]["name"]).Replace("-", " ").Capitalize(),
                        Moves[2] == null
                            ? ""
                            : ((string)Moves[2]["name"]).Replace("-", " ").Capitalize(),
                        Moves[3] == null
                            ? ""
                            : ((string)Moves[3]["name"]).Replace("-", " ").Capitalize()
                    }
                };
        }

        public virtual object ToObject()
        {
            return new
            {
                Species,
                Form,
                Ability,
                Nature = _nature.GetStringFromToken("name").Capitalize(),
                BattleStats.Level,
                BattleStats.ExperiencePoints,
                HeldItem = _item == null
                    ? ""
                    : _item.GetStringFromToken("name").Capitalize(),
                IVs = BattleStats.IVs.ToObject(),
                EVs = BattleStats.EVs.ToObject(),
                Moves = new[]
                {
                    ((string)Moves[0]["name"]).Replace("-", " ").Capitalize(),
                    Moves[1] == null
                        ? ""
                        : ((string)Moves[1]["name"]).Replace("-", " ").Capitalize(),
                    Moves[2] == null
                        ? ""
                        : ((string)Moves[2]["name"]).Replace("-", " ").Capitalize(),
                    Moves[3] == null
                        ? ""
                        : ((string)Moves[3]["name"]).Replace("-", " ").Capitalize()
                },
                TrainerID = TrainerID.ToString("D"),
                IsShiny,
                Gender,
                Nickname,
                BattleStats.Status
            };
        }

        /// <summary>
        /// Returns the exp yield of a pokemon
        /// </summary>
        /// <param name="pokemon"></param>
        /// <param name="participated"></param>
        /// <param name="isOriginalOwner"></param>
        public uint GetExpYield(PokemonBase pokemon, bool participated, bool isOriginalOwner)
        {
            double trainerMod = this is TrainerPokemon
                ? 1.5
                : 1;
            double luckyEgg = _item == null
                ? 1
                : (string)_item["name"] == "lucky-egg"
                    ? 1.5
                    : 1;
            double level = _stats.Level;
            double levelP = pokemon.BattleStats.Level;
            double participation = participated
                ? 2
                : 1;
            double tradeMod = isOriginalOwner
                ? 1
                : 1.5;

            double piece1 = (trainerMod * _baseExp * level) / (5 * participation);
            double piece2 = Math.Pow(2 * level + 10, 2.5) / Math.Pow(level + levelP + 10, 2.5);
            double piece3 = tradeMod * luckyEgg;

            return (uint)Math.Floor((piece1 * piece2 + 1) * piece3);
        }

        /// <summary>
        /// Gain exp after a batlle or from exp-increasing items
        /// </summary>
        /// <param name="exp"></param>
        /// <param name="evs"></param>
        public void GainExp(uint exp, EffortYields? yield = null)
        {
            var temp = _stats.HP;
            if (_stats.GainExp(exp, yield))
            {
                uint difference = temp - BattleStats.HP;
                var previousStages = BattleStats.CurrentStages;
                BattleStats = _stats;
                BattleStats.ImportPrevious(previousStages, _stats.HP - difference);
            }
        }

        public override string ToString()
        {
            return Nickname;
        }

        private object GetBattleStats()
        {
            return new
            {
                HP = $"{BattleStats.CurrentHP}/{BattleStats.HP}",
                BattleStats.Attack,
                BattleStats.Defense,
                BattleStats.SpecialAttack,
                BattleStats.SpecialDefense,
                BattleStats.Speed
            };
        }

        private object GetPokemonStats()
        {
            return new
            {
                HP = $"{_stats.CurrentHP}/{_stats.HP}",
                _stats.Attack,
                _stats.Defense,
                _stats.SpecialAttack,
                _stats.SpecialDefense,
                _stats.Speed
            };
        }

        private string GetURL(JToken jsonArray, string key, string value)
        {
            foreach (JObject possible in jsonArray.Children())
            {
                if ((string)possible[key]["name"] == value)
                    return (string)possible[key]["url"];
            }

            return string.Empty;
        }
    }
}
