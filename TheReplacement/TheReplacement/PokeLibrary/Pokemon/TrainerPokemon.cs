﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using TheReplacement.PokeLibrary.Pokeball.Enum;
using TheReplacement.PokeLibrary.Pokemon.PokemonCollection;
using TheReplacement.PokeLibrary.Pokemon.PokemonParts;

namespace TheReplacement.PokeLibrary.Pokemon
{
    /// <summary>
    /// Defines the class of Trainer Pokemon
    /// </summary>
    public class TrainerPokemon : PokemonBase
    {
        public static Dictionary<int, List<JObject>> StarterPokemon { get; } = new Dictionary<int, List<JObject>>()
            {
                {1, new List<JObject>()
                {
                    PokeAPI.GetForm("bulbasaur"),
                    PokeAPI.GetForm("squirtle"),
                    PokeAPI.GetForm("charmander")
                }
},
                {2, new List<JObject>()
                {
                    PokeAPI.GetForm("chikorita"),
                    PokeAPI.GetForm("totodile"),
                    PokeAPI.GetForm("cyndaquil")
                } },
                {3, new List<JObject>()
                {
                    PokeAPI.GetForm("treecko"),
                    PokeAPI.GetForm("mudkip"),
                    PokeAPI.GetForm("torchic")
                } },
                {4, new List<JObject>()
                {
                    PokeAPI.GetForm("turtwig"),
                    PokeAPI.GetForm("piplup"),
                    PokeAPI.GetForm("chimchar")
                } },
                {5, new List<JObject>()
                {
                    PokeAPI.GetForm("snivy"),
                    PokeAPI.GetForm("oshawott"),
                    PokeAPI.GetForm("tepig")
                } },
                {6, new List<JObject>()
                {
                    PokeAPI.GetForm("chespin"),
                    PokeAPI.GetForm("froakie"),
                    PokeAPI.GetForm("fennekin")
                } },
                {7, new List<JObject>()
                {
                    PokeAPI.GetForm("rowlet"),
                    PokeAPI.GetForm("popplio"),
                    PokeAPI.GetForm("litten")
                } },
                {8, new List<JObject>()
                {
                    PokeAPI.GetForm("grookey"),
                    PokeAPI.GetForm("sobble"),
                    PokeAPI.GetForm("scorbunny")
                } }
            };
        public Pokeballs Pokeball { get; private set; }
        protected TrainerPokemon() : base("") { }

        /// <summary>
        /// Constructor method for creating a starter pokemon
        /// </summary>
        /// <param name="species"></param>
        /// <param name="level"></param>
        /// <param name="form"></param>
        /// <param name="ability"></param>
        /// <param name="heldItem"></param>
        public TrainerPokemon(string species, uint level, string form = "", string ability = "", string heldItem = "") 
            : base(species: species, level: level, shinyChance: int.MaxValue, form: form, ability: ability, heldItem: heldItem)
        {
            Pokeball = Pokeballs.PokeBall;
        }

        internal TrainerPokemon(string species, string form, string ability, string nature, uint level, uint exp, string heldItem, IVs? ivs, EVs? evs, string[] moves, Guid id, bool isShiny, Pokeballs pokeball, Gender gender) 
            : base(species, form, ability, nature, level, exp, heldItem, ivs, evs, moves, id, isShiny: isShiny)
        {
            Pokeball = pokeball;
            Gender = gender;
        }

        public override object ToObject()
        {
            return new
            {
                Species,
                Form,
                Ability,
                Nature = _nature.GetStringFromToken("name").Capitalize(),
                BattleStats.Level,
                BattleStats.ExperiencePoints,
                HeldItem = _item == null
                    ? ""
                    : _item.GetStringFromToken("name").Capitalize(),
                IVs = BattleStats.IVs.ToObject(),
                EVs = BattleStats.EVs.ToObject(),
                Moves = new[]
                {
                    ((string)Moves[0]?["name"])?.Replace("-", " ").Capitalize() ?? "",
                    ((string)Moves[1]?["name"])?.Replace("-", " ").Capitalize() ?? "",
                    ((string)Moves[2]?["name"])?.Replace("-", " ").Capitalize() ?? "",
                    ((string)Moves[3]?["name"])?.Replace("-", " ").Capitalize() ?? ""
                },
                TrainerID = TrainerID.ToString("D"),
                IsShiny,
                Gender,
                Nickname,
                Pokeball,
                BattleStats.Status
            };
        }

        /// <summary>
        /// Creates a Trainer Pokemon from a Wild Pokemon
        /// </summary>
        /// <param name="pokemon"></param>
        /// <param name="pokeball"></param>
        /// <param name="nickname"></param>
        /// <param name="id"></param>
        public static TrainerPokemon CreateFromCaughtPokemon(WildPokemon pokemon, Pokeballs pokeball, string nickname, Guid id)
        {
            pokemon.BattleStats.ResetStats();
            TrainerPokemon caught
                = new TrainerPokemon(pokemon.Species, pokemon.Form, pokemon.Ability, (string)pokemon.PokemonStats.Nature["name"], pokemon.PokemonStats.Level, 0,
                pokemon.HeldItem == null
                    ? ""
                    : (string)pokemon.HeldItem["name"], pokemon.PokemonStats.IVs, pokemon.PokemonStats.EVs, null, id, pokemon.IsShiny, pokeball, pokemon.Gender);

            caught.Nickname = string.IsNullOrEmpty(nickname)
                ? caught.Nickname
                : nickname;
            if (pokeball == Pokeballs.HealBall)
            {
                caught.BattleStats.SetCurrentHP(1000);
            }
            return caught;
        }

        internal void UpdateTrainerID(Guid id)
        {
            TrainerID = id;
        }

        public static TrainerPokemon HatchedFromEgg(Egg egg)
        {
            var hatched = (egg as PokemonBase) as TrainerPokemon;
            hatched.Pokeball = egg.Pokeball;
            return hatched;
        }

        internal static TrainerPokemon LoadFromJSON(JToken pokemon)
        {
            TrainerPokemon temp = new TrainerPokemon(pokemon.GetStringFromToken("Species"),
                    pokemon.GetStringFromToken("Form"), pokemon.GetStringFromToken("Ability"),
                    pokemon.GetStringFromToken("Nature"), pokemon.GetIntFromToken("Level"),
                    pokemon.GetIntFromToken("ExperiencePoints"), pokemon.GetStringFromToken("HeldItem"),
                    new IVs(pokemon["IVs"]), new EVs(pokemon["EVs"]), GetMovesFromJSON(pokemon["Moves"]),
                    Guid.Parse(pokemon.GetStringFromToken("TrainerID")), pokemon.GetBoolFromToken("IsShiny"),
                    (Pokeballs)pokemon.GetIntFromToken("Pokeball"), (Gender)pokemon.GetIntFromToken("Gender"))
            {
                Nickname = pokemon.GetStringFromToken("Nickname")
            };
            temp.BattleStats = temp.PokemonStats;
            temp.BattleStats.SetCurrentStatus((Status)pokemon.GetIntFromToken("Status"));

            return temp;
        }

        private static string[] GetMovesFromJSON(JToken moves)
        {
            var temp = new string[4] { "", "", "", "" };
            JArray array = (JArray)moves;

            for (int i =0; i < 4; i++)
            {
                temp[i] = array[i].GetStringFromToken();
            }

            return temp;
        }
    }
}
