﻿using System;

namespace TheReplacement.PokeLibrary.Pokeball.Enum
{
    /// <summary>
    /// Defines the container for valid Pokeballs
    /// </summary>
    public enum Pokeballs
    {
        [Ball(1, 1)]
        PokeBall,

        [Ball(1.5, 1)]
        GreatBall,

        [Ball(2, 1)]
        UltraBall,

        [Ball(3, 1)]
        DuskBall,

        [Ball(3, 1)]
        MoonBall,

        [Ball(8, 1)]
        NestBall,

        [Ball(8, 1)]
        LevelBall,

        [Ball(3, 1)]
        RepeatBall,

        [Ball(3.5, 1)]
        DiveBall,

        [Ball(3.5, 1)]
        NetBall,

        [Ball(4, 1)]
        DreamBall,

        [Ball(4, 1)]
        FastBall,

        [Ball(4, 1)]
        TimerBall,

        [Ball(5, .1)]
        BeastBall,

        [Ball(5, 1)]
        LureBall,

        [Ball(5, 1)]
        QuickBall,

        [Ball(8, 1)]
        LoveBall,

        [Ball(1, 1)]
        FriendBall,

        [Ball(1, 1)]
        HealBall,

        [Ball(1, 1)]
        LuxuryBall,

        [Ball(1, 1)]
        PremierBall,

        [Ball(255, 1)]
        MasterBall
    }

    public class BallAttribute : Attribute
    {
        public double BallMultiplier { get; }
        public double FailMultiplier { get; }

        internal BallAttribute(double ball, double fail)
        {
            BallMultiplier = ball;
            FailMultiplier = fail;
        }
    }
}
