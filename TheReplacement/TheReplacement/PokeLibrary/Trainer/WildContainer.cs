﻿using TheReplacement.PokeLibrary.Pokemon;

namespace TheReplacement.PokeLibrary.Trainer
{
    /// <summary>
    /// Defines the pseudo pokemon trainer class which contains a single pokemon
    /// </summary>
    public class WildContainer : TrainerBase<WildPokemon>
    {
        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="pokemon"></param>
        /// <param name="name"></param>
        public WildContainer(WildPokemon pokemon, string name) : base(TrainerTypes.AceTrainer, name, pokemon)
        {
        }
    }
}
