﻿using Newtonsoft.Json.Linq;
using System;
using TheReplacement.PokeLibrary.Battle.Field.Enums;
using TheReplacement.PokeLibrary.Pokeball.Enum;
using TheReplacement.PokeLibrary.Pokemon;
using TheReplacement.PokeLibrary.Pokemon.PokemonCollection;
using TheReplacement.PokeLibrary.Pokemon.PokemonParts;

namespace TheReplacement.PokeLibrary.Trainer
{
    /// <summary>
    /// Defines the class for the Player Character
    /// </summary>
    public class PlayerCharacter : TrainerBase<TrainerPokemon>
    {
        private int _box;
        public PokeDex PokeDex { get; private set; }
        public PokemonBank Bank { get; private set; }
        public PokemonBox BoxToSend => Bank[_box];
        public Guid TrainerID { get; private set; }

        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="pokemon"></param>
        /// <param name="name"></param>
        public PlayerCharacter(TrainerPokemon pokemon, string name) : base(TrainerTypes.AceTrainer, name, pokemon)
        {
            _box = 0;
            Bank = new PokemonBank();
            TrainerID = Guid.NewGuid();
            foreach (var poke in Team)
            {
                poke.UpdateTrainerID(TrainerID);
            }
            PokeDex = new PokeDex();
            PokeDex.AddToPokeDex(pokemon, true);
        }

        private PlayerCharacter() : base()
        {

        }

        /// <summary>
        /// Returns an object consisting of the player's team info
        /// </summary>
        /// <returns></returns>
        public object[] GetTeamInfo()
        {
            object[] result = new object[Team.Count];

            for (int i = 0; i < result.Length; i++)
            {
                var temp = Team[i];
                result[i] = new
                {
                    temp.Species,
                    temp.Nickname,
                    temp.PokemonStats.Level,
                    HP = $"{temp.BattleStats.CurrentHP}/{temp.PokemonStats.HP}"
                };
            }

            return result;
        }

        /// <summary>
        /// Returns true if a pokemon added to the team from the bank
        /// </summary>
        /// <param name="inPartyNickname"></param>
        /// <param name="inBankNickName"></param>
        public bool GetFromPokeBank(string inBankNickName, string inPartyNickname)
        {
            int bankIndex = -1;
            int boxIndex = -1;
            for (int i = 0; i < Bank.Count; i++)
            {
                for (int j = 0; j < Bank[i].Count; j++)
                {
                    if (Bank[i][j].Nickname == inBankNickName)
                    {
                        bankIndex = i;
                        boxIndex = j;
                        break;
                    }
                }
                if (boxIndex != -1)
                {
                    break;
                }
            }

            if (boxIndex == -1)
            {
                return false;
            }

            if (string.IsNullOrEmpty(inPartyNickname))
            {
                if (Team.Count < 6)
                {
                    Team.Add(Bank[bankIndex][boxIndex]);
                    Bank[bankIndex].RemoveAt(boxIndex);
                    return true;
                }
                else
                {
                    return false;
                }
            }

            TrainerPokemon switchOut = null;
            foreach (TrainerPokemon pokemon in Team)
            {
                if (pokemon.Nickname == inPartyNickname)
                {
                    switchOut = pokemon;
                    break;
                }
            }

            if (switchOut == null)
            {
                return false;
            }

            Team[Team.IndexOf(switchOut)] = Bank[bankIndex][boxIndex];
            Bank[bankIndex][boxIndex] = switchOut;
            return true;
        }

        /// <summary>
        /// Returns true if a pokemon added to the bank from the team
        /// </summary>
        /// <param name="nickname"></param>
        public bool SendToBank(string nickname)
        {
            TrainerPokemon switchOut = null;
            foreach (TrainerPokemon pokemon in Team)
            {
                if (pokemon.Nickname == nickname)
                {
                    switchOut = pokemon;
                    break;
                }
            }

            if (switchOut == null)
            {
                Console.WriteLine($"You don't have a pokemon with the nickname {nickname}.");
                return false;
            }

            if (BoxToSend.Count == 30)
            {
                if (_box == 29)
                {
                    Console.WriteLine($"Sorry, but your bank is completely full.\n{nickname} stayed in the party.");
                    return false;
                }
                else
                {
                    Bank.Add(new PokemonBox()
                        {
                            switchOut
                        });
                    _box++;
                }
            }
            else
            {
                BoxToSend.Add(switchOut);
            }

            Team.RemoveAt(Team.IndexOf(switchOut));

            return true;
        }

        public bool RemoveFromBank(string nickname)
        {
            int bankIndex = -1;
            int boxIndex = -1;
            for (int i = 0; i < Bank.Count; i++)
            {
                for (int j = 0; j < Bank[i].Count; j++)
                {
                    if (Bank[i][j].Nickname == nickname)
                    {
                        bankIndex = i;
                        boxIndex = j;
                        break;
                    }
                }
                if (boxIndex != -1)
                {
                    break;
                }
            }

            if (boxIndex != -1)
            {
                Bank[bankIndex].RemoveAt(boxIndex);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns null if there's no pokemon in team with the given nickname
        /// </summary>
        /// <param name="nickname"></param>
        public TrainerPokemon GetPokemon(string nickname)
        {
            foreach (var pokemon in Team)
            {
                if (pokemon.Nickname.ToLower() == nickname.ToLower().Trim())
                {
                    return pokemon;
                }
            }

            return null;
        }

        /// <summary>
        /// Sets a pokemon with the given nickname as first in the Team
        /// </summary>
        /// <param name="nickname"></param>
        public void SetActivePokemon(string nickname)
        {
            if (nickname != null)
            {
                var temp = Team[0];
                for (int i = 1; i < Team.Count; i++)
                {
                    if (Team[i].Nickname.ToLower() == nickname.Trim().ToLower())
                    {
                        Team[0] = Team[i];
                        Team[i] = temp;
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Returns true if the pokemon was caught
        /// </summary>
        /// <param name="wildPokemon"></param>
        /// <param name="pokeball"></param>
        /// <param name="location"></param>
        /// <param name="turn"></param>
        public int Catch(WildPokemon wildPokemon, Pokeballs pokeball, Location location, int turn)
        {
            int shakes;
            if (pokeball == Pokeballs.MasterBall)
            {
                shakes = 4;
            }
            else
            {
                double catchNumber = GetCatchNumber(wildPokemon, pokeball, location, turn);
                Random rng = new Random();

                for (shakes = 0; shakes < 4;)
                {
                    if (rng.Next(65536) >= catchNumber)
                    {
                        return shakes;
                    }
                    shakes++;
                }
            }

            var newPokemon = TrainerPokemon.CreateFromCaughtPokemon(wildPokemon, pokeball, "", TrainerID);
            if (Team.Count < 6)
            {
                Team.Add(newPokemon);
            }
            else
            {
                if (!AddToBox(newPokemon))
                {
                    Console.WriteLine($"{newPokemon} was sent to a Box!"); /* Replace this with TextBlock */
                }
                else
                {
                    Console.WriteLine($"Team and Bank are full, so you released {newPokemon}."); /* Replace this with TextBlock */
                }
            }

            if (!PokeDex.ContainsKey(newPokemon.DexNo))
            {
                Console.WriteLine($"{newPokemon} was added to the PokeDex!"); /* Replace this with TextBlock */
                PokeDex.AddToPokeDex(newPokemon, true);
            }
            if (!PokeDex[newPokemon.DexNo].Caught)
            {
                PokeDex.AddToPokeDex(newPokemon, true);
            }
            return shakes;
        }

        internal double GetCatchNumber(WildPokemon wildPokemon, Pokeballs pokeball, Location location, int turn)
        {
            if (pokeball == Pokeballs.MasterBall)
            {
                return 65536;
            }
            TrainerPokemon yourPokemon = Team[0];
            int captureRate = wildPokemon.CaptureRate;
            uint currentHP = wildPokemon.BattleStats.CurrentHP;
            uint totalHP = wildPokemon.PokemonStats.HP;
            double statusMultiplier = wildPokemon.BattleStats.Status.GetStatusMultipler();
            bool isUltra = wildPokemon.Ability == "Beast Boost";
            bool condition = pokeball switch
            {
                Pokeballs.BeastBall => isUltra,
                Pokeballs.DiveBall => location == Location.Surfing || location == Location.Fishing,
                Pokeballs.DreamBall => wildPokemon.BattleStats.Status == Status.Asleep,
                Pokeballs.DuskBall => DateTime.Now.Hour > 18 || DateTime.Now.Hour < 7 || location == Location.Cave,
                Pokeballs.FastBall => wildPokemon.PokemonStats.Speed >= 100,
                Pokeballs.LoveBall => wildPokemon.Species != yourPokemon.Species
                    ? false
                    : wildPokemon.Gender == Gender.Male && yourPokemon.Gender == Gender.Female ||
                    wildPokemon.Gender == Gender.Female && yourPokemon.Gender == Gender.Male,
                Pokeballs.LureBall => location == Location.Fishing,
                Pokeballs.MoonBall => wildPokemon.EvolutionChain == "https://pokeapi.co/api/v2/evolution-chain/12/" ||
                    wildPokemon.EvolutionChain == "https://pokeapi.co/api/v2/evolution-chain/13/" ||
                    wildPokemon.EvolutionChain == "https://pokeapi.co/api/v2/evolution-chain/14/" ||
                    wildPokemon.EvolutionChain == "https://pokeapi.co/api/v2/evolution-chain/148/" ||
                    wildPokemon.EvolutionChain == "https://pokeapi.co/api/v2/evolution-chain/265/",
                Pokeballs.RepeatBall => PokeDex.ContainsKey(wildPokemon.DexNo)
                    ? PokeDex[wildPokemon.DexNo].Caught
                    ? true
                    : false
                    : false,
                _ => true,
            };
            double ballMultiplier = condition
                ? pokeball.GetBallMultipler(isUltra, wildPokemon.PokemonStats.Level, yourPokemon.PokemonStats.Level, turn)
                : pokeball.GetFailMultipler(isUltra);

            double catchValue = Math.Floor((((3 * totalHP - 2 * currentHP) * captureRate * ballMultiplier) / (3 * totalHP) * statusMultiplier));
            return catchValue > 255 ? 65536 : Math.Floor((65536 / Math.Pow(255 / catchValue, 0.1875)));
        }

        public static PlayerCharacter LoadFromSave(JObject json)
        {
            var team = json["Team"];
            var bank = json["Bank"];
            JArray dex = (JArray)json["PokeDex"];
            var tempTrainer = new PlayerCharacter()
            {
                Name = json.GetStringFromToken("Name"),
                TrainerType = (TrainerTypes)json.GetIntFromToken("TrainerType"),
                TrainerID = Guid.Parse(json.GetStringFromToken("TrainerID")),
                Team = LoadTeam(team)
            };

            tempTrainer._box = 0;
            tempTrainer.Bank = new PokemonBank();
            foreach (var box in bank)
            {
                foreach (var pokemon in box)
                {
                    tempTrainer.AddToBox(TrainerPokemon.LoadFromJSON(pokemon));
                }
            }

            tempTrainer.PokeDex = new PokeDex();
            foreach (var pokemon in dex.Children<JObject>())
            {
                tempTrainer.PokeDex.AddToPokeDex(pokemon);
            }

            return tempTrainer;
        }

        public object ToObject()
        {
            return new
            {
                Name,
                TrainerType,
                TrainerID,
                Team = Team.ToObject(),
                Bank = Bank.ToObject(),
                PokeDex = PokeDex.ToObject()
            };
        }

        public object[] GetPokeDex()
        {
            return PokeDex.ToObject();
        }

        public void AddSeenToDex(WildPokemon pokemon)
        {
            PokeDex.AddToPokeDex(pokemon);
        }

        public object[] GetPokeBank()
        {
            return Bank.ToObject();
        }

        private static TrainerTeam<TrainerPokemon> LoadTeam(JToken json)
        {
            var temp = new TrainerTeam<TrainerPokemon>();
            int index = 0;
            foreach (var pokemon in json)
            {
                temp.Add(TrainerPokemon.LoadFromJSON(pokemon));
                index++;
            }

            return temp;
        }

        /// <summary>
        /// Returns true if the Pokemon Bank isn't full
        /// </summary>
        /// <param name="pokemon"></param>
        /// <returns></returns>
        internal bool AddToBox(TrainerPokemon pokemon)
        {
            if (BoxToSend.Count == 30)
            {
                if (_box == 29)
                {
                    Console.WriteLine($"Sorry, but your bank is completely full.\nYou released {pokemon}");
                    return false;
                }
                else
                {
                    Bank.Add(new PokemonBox()
                        {
                            pokemon
                        });
                    _box++;
                }
            }
            else
            {
                BoxToSend.Add(pokemon);
            }

            return true;
        }
    }
}
