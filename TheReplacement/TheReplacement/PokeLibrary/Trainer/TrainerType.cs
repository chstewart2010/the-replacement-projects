﻿using System;

namespace TheReplacement.PokeLibrary.Trainer
{
    /// <summary>
    /// Defines the contains for pokemon trainer types
    /// </summary>
    public enum TrainerTypes
    {
        [TrainerType("Ace Trainer")]
        AceTrainer,
        [TrainerType("Bug Catcher")]
        BugCatcher,
        [TrainerType("Poke Freak")]
        PokeFreak,
        [TrainerType("Ninja Kid")]
        NinjaKid,
        Hiker,
        Gambler,
        Psychic,
        Gentleman,
        Lady,
        [TrainerType("Black Belt")]
        BlackBelt,
        [TrainerType("Poke Fan")]
        PokeFan,
        Youngster,
        Trainer,
        [TrainerType("Gym Leader")]
        GymLeader,
        [TrainerType("Ace Trainer")]
        Elite4,
        [TrainerType("Champion")]
        Champion
    }

    /// <summary>
    /// Defines the modified name for the Trainer Type
    /// </summary>
    public class TrainerTypeAttribute : Attribute
    {
        public string Name { get; }

        public TrainerTypeAttribute(string name)
        {
            Name = name;
        }
    }
}