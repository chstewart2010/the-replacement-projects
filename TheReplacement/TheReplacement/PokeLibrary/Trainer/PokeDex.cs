﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using TheReplacement.PokeLibrary.Pokemon;
using static TheReplacement.PokeLibrary.Trainer.PokeDex;

namespace TheReplacement.PokeLibrary.Trainer
{
    public class PokeDex : Dictionary<uint, PokeDexEntry>
    {
        internal PokeDex(): base(807)
        {

        }

        internal void AddToPokeDex(PokemonBase pokemon, bool caught = false)
        {
            if (!ContainsKey(pokemon.DexNo))
            {
                var entry = new PokeDexEntry(pokemon);
                if (caught)
                {
                    entry.Caught = true;
                }
                Add(pokemon.DexNo, entry);
            }
            else
            {
                var entry = this[pokemon.DexNo];
                if (caught && !entry.Caught)
                {
                    entry.PokemonCaught();
                }
                entry.AddForm(pokemon.Form);
            }
        }

        internal void AddToPokeDex(JToken pokemon)
        {
            uint index = pokemon.GetIntFromToken("DexNo");
            if (!ContainsKey(index))
            {
                Add(index, new PokeDexEntry(pokemon));
            }
            else
            {
                var entry = this[index];
                foreach (var form in PokeDexEntry.GetFormsFromJSON(pokemon["forms"]))
                {
                    entry.AddForm(form);
                }
            }
        }

        public object[] ToObject()
        {
            object[] temp = new object[Count];
            int index = 0;
            for (uint i = 0; i < 807; i++)
            {
                if (index == Count)
                {
                    break;
                }
                if (ContainsKey(i))
                {
                    temp[index] = new
                    {
                        this[i].DexNo,
                        this[i].Species,
                        Forms = this[i].Forms.ToArray(),
                        this[i].Caught
                    };
                    index++;
                }
            }

            return temp;
        }

        public struct PokeDexEntry
        {
            public uint DexNo { get; }
            public string Species { get; }
            public List<string> Forms { get; }
            public bool Caught { get; internal set; }

            internal PokeDexEntry(PokemonBase pokemon)
            {
                DexNo = pokemon.DexNo;
                Species = pokemon.Species;
                Forms = new List<string>();
                Forms.Add(pokemon.Form);
                Caught = false;
            }

            internal PokeDexEntry(JToken entry)
            {
                DexNo = entry.GetIntFromToken("DexNo");
                Species = entry.GetStringFromToken("Species");
                Forms = GetFormsFromJSON(entry["Forms"]);
                Caught = entry.GetBoolFromToken("Caught");
            }

            internal static List<string> GetFormsFromJSON(JToken forms)
            {
                JArray array = (JArray)forms;
                var temp = new List<string>();

                for (int i = 0; i < array.Count; i++)
                {
                    temp.Add(array[i].GetStringFromToken());
                }

                return temp;
            }

            /// <summary>
            /// Set a pokemon entry's caught status to true <para />
            /// Should never be able to be set back to false once true
            /// </summary>
            internal void PokemonCaught()
            {
                Caught = true;
            }

            internal void AddForm(string form)
            {
                if (!Forms.Contains(form))
                {
                    Forms.Add(form);
                }
            }
        }
    }
}
