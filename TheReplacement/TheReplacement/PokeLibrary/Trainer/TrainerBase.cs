﻿using TheReplacement.PokeLibrary.Pokemon;
using TheReplacement.PokeLibrary.Pokemon.PokemonCollection;

namespace TheReplacement.PokeLibrary.Trainer
{
    /// <summary>
    /// Defines the base class for creating Pokemon Trainers
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TrainerBase<T> where T : PokemonBase
    {
        public TrainerTypes TrainerType { get; private protected set; }
        public TrainerTeam<T> Team { get; private protected set; }
        public string Name { get; private protected set; }

        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <param name="pokemon"></param>
        public TrainerBase(TrainerTypes type, string name, params T[] pokemon)
        {
            TrainerType = type;
            Name = name;
            Team = new TrainerTeam<T>(pokemon);
        }

        private protected TrainerBase()
        {

        }

        /// <summary>
        /// Returns an object consisting of information from a requested pokemon
        /// </summary>
        /// <param name="nickname"></param>
        public object GetPokemonInfo(string nickname)
        {
            foreach (var pokemon in Team)
            {
                if (pokemon.Nickname.ToLower() == nickname.ToLower().Trim())
                {
                    return pokemon.GetPokemonInfo(false);
                }
            }

            return new object();
        }
    }
}
