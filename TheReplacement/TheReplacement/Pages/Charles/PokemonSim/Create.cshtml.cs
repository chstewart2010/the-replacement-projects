﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using TheReplacement.Data;

namespace TheReplacement.Pages.Charles.PokemonSim
{
    public class CreateModel : PageModel
    {
        private readonly TheReplacementContext _context;

        public CreateModel(TheReplacementContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public GameData GameData { get; set; }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.GameData.Add(GameData);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
