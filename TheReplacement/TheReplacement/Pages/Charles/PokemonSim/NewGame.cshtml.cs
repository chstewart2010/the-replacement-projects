using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using System.Threading.Tasks;
using TheReplacement.Data;
using TheReplacement.PokeLibrary.Pokemon;
using TheReplacement.PokeLibrary.Trainer;

namespace TheReplacement.Pages.Charles.PokemonSim
{
    public class NewGameModel : PageModel
    {
        //readonly Random _rng = new Random();
        //public TrainerPokemon PokemonGet { get; set; }
        private readonly TheReplacementContext _context;
        public PlayerCharacter Character { get; set; }

        [BindProperty]
        public GameData GameData { get; set; }

        public NewGameModel(TheReplacementContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        //public void OnGet()
        //{
        //    //PokemonGet = new TrainerPokemon(_rng.Next(1, 808).ToString(), _rng.Next(1, 100), "random", "random", "");
        //}

        public async Task<IActionResult> OnPostAsync(string species, string nickname)
        {
            var starter = new TrainerPokemon(species, 5);
            starter.Nickname = string.IsNullOrEmpty(nickname)
                ? starter.Species
                : nickname;
            Character = new PlayerCharacter(starter, GameData.Name);

            if (!ModelState.IsValid)
            {
                return Page();
            }

            GameData.SaveData = JsonConvert.SerializeObject(Character.ToObject());
            _context.GameData.Add(GameData);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
