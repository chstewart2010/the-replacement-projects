﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheReplacement.Pages.Charles.PokemonSim
{
    public class GameData
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string SaveData { get; set; }
    }
}
