using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TheReplacement.Data;
using TheReplacement.PokeLibrary.Battle.Field.Enums;
using TheReplacement.PokeLibrary.Pokeball.Enum;
using TheReplacement.PokeLibrary.Pokemon;
using TheReplacement.PokeLibrary.Pokemon.PokemonParts;
using TheReplacement.PokeLibrary.Trainer;

namespace TheReplacement.Pages.Charles.PokemonSim
{
    public class CatchModel : PageModel
    {
        private readonly TheReplacementContext _context;
        private readonly Random _rng = new Random();
        public int TurnsPassed { get; set; } = 0;
        public PlayerCharacter Character { get; set; }
        public WildPokemon WildPokemon { get; set; }
        public int NumberOfShakes { get; private set; } = -1;
        public Location Location { get; } =  (Location)(new Random().Next(7));
        public Weather Weather { get; } = (Weather)(new Random().Next(7));

        public CatchModel(TheReplacementContext context)
        {
            _context = context;
        }

        [BindProperty]
        public GameData GameData { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id, string wild, int? turns, int? shakes)
        {
            if (id == null)
            {
                return NotFound();
            }

            GameData = await _context.GameData.FirstOrDefaultAsync(m => m.ID == id);
            Character = PlayerCharacter.LoadFromSave(JObject.Parse(GameData.SaveData));
            if (string.IsNullOrEmpty(wild))
            {
                WildPokemon = new WildPokemon(new Random().Next(1, 808).ToString(), "random", "random", 50);
            }
            else
            {
                WildPokemon = WildPokemon.LoadFromJSON(JObject.Parse(wild));
                WildPokemon.BattleStats.SetCurrentHP((uint)_rng.Next(1, (int)WildPokemon.BattleStats.CurrentHP + 1));
                WildPokemon.BattleStats.SetCurrentStatus((Status)(_rng.Next(1, 8)));
            }
            if(turns != null)
            {
                TurnsPassed = turns.Value;
            }
            if(shakes != null)
            {
                NumberOfShakes = shakes.Value;
            }

            if (GameData == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync(int turnsPassed, string wild, string pokeball, string character)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            WildPokemon wildPokemon = WildPokemon.LoadFromJSON(JObject.Parse(wild));
            Character = PlayerCharacter.LoadFromSave(JObject.Parse(GameData.SaveData));
            NumberOfShakes = Character.Catch(wildPokemon, Enum.Parse<Pokeballs>(pokeball), Location, TurnsPassed);
            if (NumberOfShakes == 4)
            {
                GameData.Name = Character.Name;
                GameData.SaveData = JsonConvert.SerializeObject(Character.ToObject());
                _context.Attach(GameData).State = EntityState.Modified;
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GameDataExists(GameData.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToPage("./Details", new { id = GameData.ID });
            }
            else
            {
                TurnsPassed = turnsPassed + 1;
                return TurnsPassed == 15
                    ? RedirectToPage("./Index")
                    : RedirectToPage("./Catch", new { id = GameData.ID, wild, turns = TurnsPassed, shakes = NumberOfShakes });
            }
        }

        private bool GameDataExists(int id)
        {
            return _context.GameData.Any(e => e.ID == id);
        }
    }
}
