﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using TheReplacement.Data;
using TheReplacement.PokeLibrary.Trainer;

namespace TheReplacement.Pages.Charles.PokemonSim
{
    public class DetailsModel : PageModel
    {
        private readonly TheReplacementContext _context;
        public PlayerCharacter Character { get; set; }

        public DetailsModel(TheReplacementContext context)
        {
            _context = context;
        }

        public GameData GameData { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            GameData = await _context.GameData.FirstOrDefaultAsync(m => m.ID == id);

            if (GameData == null)
            {
                return NotFound();
            }

            Character = PlayerCharacter.LoadFromSave(JObject.Parse(GameData.SaveData));
            return Page();
        }
    }
}
