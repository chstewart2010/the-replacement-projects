﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using TheReplacement.Data;

namespace TheReplacement.Pages.Charles.PokemonSim
{
    public class EditModel : PageModel
    {
        private readonly TheReplacementContext _context;

        public EditModel(TheReplacementContext context)
        {
            _context = context;
        }

        [BindProperty]
        public GameData GameData { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            GameData = await _context.GameData.FirstOrDefaultAsync(m => m.ID == id);

            if (GameData == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(GameData).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GameDataExists(GameData.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool GameDataExists(int id)
        {
            return _context.GameData.Any(e => e.ID == id);
        }
    }
}
