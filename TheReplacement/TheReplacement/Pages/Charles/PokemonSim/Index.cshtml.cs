﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using TheReplacement.Data;

namespace TheReplacement.Pages.Charles.PokemonSim
{
    public class IndexModel : PageModel
    {
        private readonly TheReplacement.Data.TheReplacementContext _context;

        public IndexModel(TheReplacement.Data.TheReplacementContext context)
        {
            _context = context;
        }

        public IList<GameData> GameData { get;set; }

        public async Task OnGetAsync()
        {
            GameData = await _context.GameData.ToListAsync();
        }
    }
}
