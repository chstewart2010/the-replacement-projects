﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TheReplacement.PokeLibrary.Pokemon;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TheReplacement.PokeLibrary.Trainer;
using TheReplacement.PokeLibrary.Pokeball.Enum;
using TheReplacement.PokeLibrary.Battle.Field.Enums;
using TheReplacement.PokeLibrary.Pokemon.PokemonParts;
using TheReplacement.PokeLibrary;
using Newtonsoft.Json.Linq;

namespace TheReplacement.Pages.Charles
{
    public class PokemonCatchrateCalculatorModel : PageModel
    {
        public JToken SpeciesList = PokeAPI.GetSpecies("?offset=0&limit=807")["results"];
        public StatInfo CurrentStatus { get; set; }
        public WildPokemon Pokemon { get; set; }
        public string Chance { get; set; }
        public double ShinyChance { get; set; }
        public void OnGet()
        {

        }

        public void OnPost(string speciesY, uint levelY, string species, uint currentHP, uint level, int turnsPassed, string status, string pokeball, string location, string dexStatus, int numBattled)
        {
            PlayerCharacter character = new PlayerCharacter(new TrainerPokemon(speciesY, levelY), "Catch Calculator");
            double numerator = 1;
            numerator += dexStatus == "Completed"
                ? 2
                : 0;
            numerator += numBattled >= 500
                ? 5
                : numBattled >= 300
                    ? 4
                    : numBattled >= 200
                        ? 3
                        : numBattled >= 100
                            ? 2
                            : numBattled >= 50
                                ? 1
                                : 0;
            ShinyChance = Math.Round(numerator / 4096 * 100, 2);
            Pokemon = new WildPokemon(species, "", "", level, shinyChance: (int)(4096 / numerator));
            if (dexStatus == "Completed")
            {
                uint i = 0;
                foreach (var result in SpeciesList)
                {
                    i++;
                    if (i == character.Team[0].DexNo)
                    {

                        continue;
                    }
                    string name = result.GetStringFromToken("name");
                    var pokemon = JObject.Parse($"{{Species: \"{name}\", DexNo: {i}, Caught: true, Forms: []}}");
                    character.PokeDex.Add(i, new PokeDex.PokeDexEntry(pokemon));
                }
            }
            else if (dexStatus == "Yes")
            {
                character.PokeDex.AddToPokeDex(Pokemon, true);
            }
            Pokeballs pokeballSelected = Enum.Parse<Pokeballs>(pokeball);
            Location locationSelected = Enum.Parse<Location>(location);
            Status statusSelected = Enum.Parse<Status>(status);
            CurrentStatus = new StatInfo(statusSelected, locationSelected, pokeballSelected, turnsPassed, Pokemon.Types, dexStatus, numBattled);
            Pokemon.BattleStats.SetCurrentStatus(statusSelected);
            Pokemon.BattleStats.SetCurrentHP(currentHP);
            double catchNumber = character.GetCatchNumber(Pokemon, pokeballSelected, locationSelected, turnsPassed);
            Chance = catchNumber == 65536 ? "100%" : $"{Math.Round(Math.Pow(catchNumber / 65536, 4) * 100, 4)}%";
        }

        public struct StatInfo
        {
            public Status Status { get; set; }
            public Location Location { get; set; }
            public Pokeballs Pokeball { get; set; }
            public int TurnsPassed { get; set; }
            public string Typing { get; set; }
            public string DexCompletion { get; set; }
            public int NumberBattled { get; set; }

            public StatInfo(Status status, Location location, Pokeballs pokeball, int turnsPassed, Types typing, string dexStatus, int numberBattled)
            {
                Status = status;
                Location = location;
                Pokeball = pokeball;
                TurnsPassed = turnsPassed;
                string types = string.Empty;
                foreach (Types value in Enum.GetValues(typeof(Types)))
                {
                    if (typing.HasFlag(value))
                    {
                        types += $"{value}/";
                    }
                }
                Typing = types.Trim('/');
                DexCompletion = dexStatus == "Completed"
                    ? dexStatus
                    : "Incomplete";
                NumberBattled = numberBattled;
            }
        }
    }
}