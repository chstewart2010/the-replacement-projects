from random import *


class HashClass:
    """Provides a collection of methods for encrypting strings"""

    @classmethod
    def convert(cls, hash_key: int, value: str) -> dict:
        """Returns a hex array representing the inputted string value

        Parameters
        ---
        hash_key : int
            The seed key for the hash randomizer
        value : str
            The string value to encrypt
        """
        hash_table = cls.__init_hash__(hash_key)
        if not isinstance(value, str):
            value = str(value)
        temp = []
        for c in value:
            temp.append(hash_table[c])
        return {"key": hash_key, "hash": temp}

    @classmethod
    def deconvert(cls, hash_key: int, value: list) -> str:
        """Returns a string representing the inputted hex array

        Parameters
        ---
        hash_key : int
            The seed key for the hash randomizer
        value : str
            The hex array value to decrypt
        """
        hash_table = cls.__init_hash__(hash_key)
        if not isinstance(value, list):
            return None
        temp = ""
        keys = list(hash_table.keys())
        values = list(hash_table.values())
        for h in value:
            temp += keys[values.index(h)]
        return temp

    @classmethod
    def __init_hash__(cls, key: int) -> dict:
        """Returns a random hash map based on a seed key

        Parameter
        ---
        key : int
            The seed key for the hash randomizer
        """
        valid_vals = list(range(256))
        hash_table = {}
        for val in range(256):
            position = Random(key).randrange(0,len(valid_vals))
            hash_table[chr(val)] = hex(valid_vals[position])
            valid_vals.pop(position)
        return hash_table
