from enum import IntEnum

class Suit(IntEnum):
    Spades = 1
    Diamonds = 2
    Clubs = 3
    Hearts = 4
