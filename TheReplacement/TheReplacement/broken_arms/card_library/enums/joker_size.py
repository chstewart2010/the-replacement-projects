from enum import IntEnum

class JokerSize(IntEnum):
    Little = 1
    Big = 2
