from card_library.deck_model.card import Card
from card_library.deck_model.standard_card import StandardCard
from card_library.deck_model.joker_card import JokerCard
from card_library.enums.suit import Suit
from card_library.enums.value import Value
from card_library.enums.joker_size import JokerSize
from enum import Enum
import random

class DeckBuilder:
    """Provides a collection of methods to create a deck of cards"""

    @classmethod
    @property
    def shuffled_standard_deck(cls) -> list[StandardCard]:
        """Returns a randomly shuffled deck of cards"""
        return cls.get_shuffle(cls.standard_deck)

    @classmethod
    @property
    def spades_deck(cls) -> list[Card]:
        """Returns a randomly shuffled spades deck"""
        arr = []
        for size in JokerSize:
            arr.append(JokerCard(size))
        for value in Value:
            for suit in Suit:
                if value == Value.Two and suit in [Suit.Hearts, Suit.Diamonds]:
                    continue
                arr.append(StandardCard(value, suit))
        return cls.get_shuffle(arr)

    @classmethod
    @property
    def standard_deck(cls) -> list[StandardCard]:
        """Returns an unshuffled deck of cards"""
        arr = []
        for value in Value:
            for suit in Suit:
                arr.append(StandardCard(value, suit))
        return arr

    @classmethod
    def get_shuffle(cls, deck: list[Card]) -> list[Card]:
        """Returns a shuffled deck of cards

        Parameter
        ---
        deck : list[Card]
            The deck of cards to shuffle.
        
        Raises
        ---
        TypeError
            If the [deck] parameter is not a valid list of cards
        """
        arr = []
        for i in range(52):
            position = random.randrange(0, len(deck))
            card = deck[position]
            if not isinstance(card, Card):
                raise TypeError(f"{card} is not a valid Card")
            arr.append(deck[position])
            deck.pop(position)
        
        return arr
