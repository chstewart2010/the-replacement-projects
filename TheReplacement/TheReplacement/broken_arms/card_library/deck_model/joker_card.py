from card_library.deck_model.card import Card
from card_library.enums.joker_size import JokerSize

class JokerCard(Card):
    """Represent a Joker card in a standard 52-card deck"""
    
    def __init__(self, size: JokerSize):
        """Initializes the joker card

        Parameters
        ---
        size : JokerSize
            The size of the Joker
        """
        self.__size = size
    
    def __repr__(self) -> str:
        """Returns a string representation of the joker card"""
        return  f"{self.__size.name} Joker"
    
    @property
    def value(self) -> JokerSize:
        """Gets the cards rank"""
        return self.__size
