from abc import ABC

class Card(ABC):
    """Represent an abstract card"""
    
    def __init__(self):
        """Initializes the card"""
        pass
    
    @property
    def value(self):
        """Gets the card's value"""
        pass
    
    @property
    def png(self) -> str:
        """Returns the path to the png of the card"""
        return f"{self}.png"
