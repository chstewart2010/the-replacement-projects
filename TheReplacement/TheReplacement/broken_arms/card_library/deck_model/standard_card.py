from card_library.enums.suit import Suit
from card_library.enums.value import Value
from card_library.deck_model.card import Card

class StandardCard(Card):
    """Represent a card in a standard 52-card deck"""
    
    def __init__(self, value: Value, suit: Suit):
        """Initializes the standard card

        Parameters
        ---
        value : Value
            The card's rank
        suit : Suit
            The card's suit
        """
        self.__value = value
        self.__suit = suit
    
    def __repr__(self) -> str:
        """Returns a string representation of the standard card"""
        return f"{self.__value.name} of {self.__suit.name}"
    
    @property
    def value(self) -> Value:
        """Gets the cards rank"""
        return self.__value
