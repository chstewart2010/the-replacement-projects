from flask import Flask, request, jsonify
from api.blackjack import Blackjack
from blackjack_library.player import BlackjackPlayer
from blackjack_library.enums.game_state import GameState
from blackjack_library.enums.game_option import GameOption
from uuid import UUID

app = Flask(__name__)
app.config["DEBUG"] = True

games: dict[str, Blackjack] = {}
blackjack_resource = "/api/v1/resources/blackjack"

@app.route("/", methods=["GET", "POST"])
def home():
    """API home"""
    if request.method == "GET":
        return get_response("Result")
    if request.method == "POST":
        print(request.mimetype)
        print(request.json)
        return get_response(request.get_json())

@app.route(f"{blackjack_resource}", methods=["POST"])
def create():
    """Initializes a new game of Blackjack"""
    print("hey")
    game = Blackjack()
    if not game.valid:
        del game
        return get_fail({"fail_message": "Failed to init a new Blackjack game"})
    games[game.uuid] = game
    game_data = get_game_data(game)
    print(game_data)
    
    return get_response(game_data)

@app.route(f"{blackjack_resource}/start/<uuid>", methods=["POST"])
def start(uuid):
    """Set the game state to InGame

    Parameter
    ---
    uuid : str
        Blackjack game's uuid
    """
    game = games.get(uuid)
    if game:
        response = game.start()
        games[game.uuid] = game
        return get_response(response)
    
    return get_fail({"fail_message": f"Invalid uuid: {uuid}"})

@app.route(f"{blackjack_resource}/<uuid>", methods=["GET", "DELETE"])
def get_delete_game(uuid):
    """Gets or deletes a Blackjack game

    Parameter
    ---
    uuid : str
        Blackjack game's uuid
    """
    game = games.get(uuid)
    response = None
    print(request.method)
    if request.method == "GET":
        if game:
            game_data = get_game_data(game)
            response = get_response(game_data)
    else:
        if game and GameState[game.state] == GameState.Empty:
            game_data = get_game_data(game)
            games.pop(uuid)
            del game
            response = get_response(game_data)
    
    return response or get_fail({"fail_message": f"Invalid uuid: {uuid}"})

@app.route(f"{blackjack_resource}/add/<uuid>/<name>", methods=["POST"])
def add_player(uuid, name):
    """Adds a new player to a Blackjack game

    Parameters
    ---
    uuid : str
        Blackjack game's uuid
    name : str
        New player's name
    """
    game = games.get(uuid)
    if not game:
        return get_fail({"fail_message": f"Invalid uuid: {uuid}"})

    if not (game.state in [GameState.Queuing, GameState.Empty]):
        return get_fail({"fail_message": f"Game with uuid \"{game.uuid}\" is not accepting players."})
    
    response = game.add_player(name)
    games[uuid] = game
    return get_response(response)

@app.route(f"{blackjack_resource}/<uuid>/<name>", methods=["GET", "DELETE"])
def get_delete_player(uuid,name):
    """Gets or delete a player from a Blackjack game

    Parameters
    ---
    uuid : str
        Blackjack game's uuid
    name : str
        The player to get or delete
    """
    game = games.get(uuid)
    if not game:
        return get_fail({"fail_message": f"Invalid uuid: {uuid}"})
    if request.method == "GET":
        return get_response(game.get_player(name))
    else:
        response = game.remove_player(name)
        games[uuid] = game
        return get_response(response)

@app.route(f"{blackjack_resource}/bet/<uuid>/<name>/<int:money>", methods=["POST"])
def set_bet(uuid, name, money: int):
    """Function to set a player's bet
    
    Parameters
    ---
    name : str
        The player to set their bet
    money : float
        The money the player wants to bet
    """
    game = games.get(uuid)
    if not game:
        return get_fail({"fail_message": f"Invalid uuid: {uuid}"})
    if game.state != GameState.InGame:
        return get_fail({"fail_message": f"Game with uuid \"{game.uuid}\" is not accepting bets."})
    
    response = game.set_bet(name, money)
    games[uuid] = game
    return get_response(response)

@app.route(f"{blackjack_resource}/deal/<uuid>/<name>/<option>", methods=["POST"])
def deal(uuid, name, option):
    """Function to set a player's bet
    
    Parameters
    ---
    name : str
        The player who's turn it is
    option : str
        Whether to Hit or Stand
    """
    game = games.get(uuid)
    if not game:
        return get_fail({"fail_message": f"Invalid uuid: {uuid}"})
    
    try:
        response = game.deal_to_player(name, GameOption[option])
        games[uuid] = game
        return get_response(response)
    except:
        return get_fail({"fail_message": f"Invalid option: {option}"})

def get_in_game(uuid: str) -> Blackjack:
    """Returns the requested game if state is InGame

    Parameter
    ---
    uuid : str
        Blackjack game's uuid
    """
    game = games.get(uuid)
    if not game:
        return get_fail(f"invalid uuid: \"{uuid}\"")
    if game.state == GameState.InGame:
        return game
    
    return None

def get_game_data(game: Blackjack) -> dict:
    """Returns a `dict` representing a Blackjack game

    Parameter
    ---
    game : Blackjack
        Blackjack game
    """
    game_data = {"id": game.uuid, "state": game.state, "player": []}
    for player in game.players:
        game_data["player"].append({
            player.name: {
                "hand": player.hand,
                "money": player.money_pretty,
                "winnings": player.winnings_pretty
            }
        })
    
    return game_data

def get_fail(fail_message=None):
    """Returns a fail response

    Parameter
    fail_message : any
        message to send
    """
    return get_response(fail_message or "raggle fraggle")

def get_response(data):
    """Return a valid response based on expect return data"""
    response = jsonify(data)
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response

app.run()