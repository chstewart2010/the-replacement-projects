from card_library.deck_model.card import Card
from card_library.deck_model.deck_builder import DeckBuilder
from blackjack_library.enums.start_option import StartOption
from blackjack_library.enums.game_option import GameOption
from blackjack_library.player import BlackjackPlayer
from crypto.hash_test import HashClass
from utils.common import Common
from os import path
import json
import random
import sys

class Blackjack:
    """Provides the abiility to play a game of Blackjack via command line"""

    __bets: dict = {}
    __deck: list[Card] = DeckBuilder.shuffled_standard_deck
    __players: dict[BlackjackPlayer] = {}
    __saveData: dict = {}
    __BET_MINIMUM: int = 50
    __BALANCE_MINIMUM: int = 100
    __DEALER = BlackjackPlayer("__DEALER", 1000, 1000)
    __PNG_ROOT = ".\\pngs\\standard_deck"
    __KEY = 10

    @classmethod
    def start(cls):
        """Main function to play blackjack"""
        if not Common.validate_deck(cls.__PNG_ROOT, cls.__deck):
            return

        cls.__saveData = Common.load()
        cls.__main_menu__()

    @classmethod
    def __main_menu__(cls, option: StartOption):
        """The main menu directory for the blackjack game"""
        option: StartOption
        while True:
            try:
                option = cls.__get_start_option__("Would you like to play a game of Blackjack?", "0. New Game\n1. Continue\n2. No\n3. Account\n")
            except EOFError:
                # Intercept Ctrl-Z and gracefully terminate the game
                option = StartOption.No
            if option == StartOption.NewGame:
                if not cls.__new_character__(True):
                    # return to the main menu
                    continue
                cls.__play_rounds__()
            elif option == StartOption.Continue:
                if not cls.__return_character__(True):
                    # return to the main menu
                    continue
                cls.__play_rounds__()
            elif option == StartOption.Account:
                # edit user password
                cls.__account__()
            else:
                # save and leave
                Common.save(cls.__saveData)
                break

    @classmethod
    def __play_rounds__(cls):
        """Function to start a new game of blackjack"""
        # exit game if no players are found
        if len(cls.__players) == 0:
            return
        option: StartOption
        option_string: str
        removal_list: list[BlackjackPlayer]
        while True:
            print()
            removal_list = []
            # option to continue or leave for returning players
            for player in cls.__players.values():
                print(f"Player: {player.name}")
                print(f"Player Balance: {player.money_pretty}")
                print(f"Total Winnings: {player.winnings_pretty}")
                try:
                    option = cls.__get_start_option__("Let's play", "1. Continue\n2. No\n")
                except EOFError:
                    option = StartOption.No

                if option == StartOption.Continue:
                    if player.money < cls.__BALANCE_MINIMUM:
                        print("Bruh, you broke." if player.money >= 0 else "Bruh, you in debt")
                        cls.__saveData[player.name]["money"] = 500
                        player.set_money(500, player.winnings)
                        continue
                elif option == StartOption.No:
                    cls.__saveData[player.name]["money"] = player.money if player.money > 100 else 500
                    cls.__saveData[player.name]["earnings"] = player.winnings
                    removal_list.append(player.name)
                    continue
                else:
                    print(f"Invalid command {option_string}")
            
            for player in removal_list:
                cls.__players.pop(player)
            
            # option to add more players if we have fewer that 4
            if len(cls.__players) < 4:
                cls.__more_players__()

            # quit if we have no players
            # re-initialize deck if we are starting to run low
            if len(cls.__players) > 0:
                if len(cls.__deck) < 5 * len(cls.__players):
                    cls.__deck = DeckBuilder.shuffled_standard_deck
                cls.__play_round__(cls.__players.values())
            else:
                break

    @classmethod
    def __play_round__(cls, players: list[BlackjackPlayer]):
        """Function to start a new round of blackjack
        
        
        Parameter
        ---
        players : list[BlackjackPlayer]
            The group of players in this round of blackjack
        """
        # set the player bets
        cls.__bets = {}
        for player in players:
            cls.__set_bet__(player)
        cls.__deal_card__(2)

        # blackjack check on players
        for player in players:
            if player.hand_value == 21:
                print()
                if player.hand_value > cls.__DEALER.hand_value:
                    print(f"{player.name} hit blackjack")
                    player.add_money(cls.__bets[player.name] * 1.5)
                    player.blackjack = True
                if player.hand_value == cls.__DEALER.hand_value:
                    print("Tie")
                    player.add_money(cls.__bets[player.name] * 0)

        # blackjack check on Dealer
        if cls.__DEALER.hand_value == 21:
            cls.__view_hands__(players)
            cls.__view_hand__(cls.__DEALER)
            for player in players:
                if player.hand_value < cls.__DEALER.hand_value:
                    player.add_money(cls.__bets[player.name] * -1)
            print("Dealer hit blackjack")
            cls.__game_over__(players)
            return
        
        # players' turns
        for player in players:
            if player.hand_value == 21:
                continue
            option: GameOption
            total_busts = 0
            player_stand = False
            print()
            print()

            while True:
                if not player_stand:
                    cls.__view_hands__(players)
                    print(f"{player.name} Hand: {player.hand_value}")
                    cls.__view_hand__(player)
                    print("\nDealer Hand")
                    print(f"{cls.__DEALER.hand[0]}\n<hidden>")
                    try:
                        option = cls.__get_game_option__()
                    except EOFError:
                        # intercept attempts to Ctrl-Z out of the game
                        print("Oh no, it's too late for that, mah dude.")
                        continue
                    if option == GameOption.Hit:
                        cls.__deal_to_player__(player)
                        if player.hand_value> 20:
                            print(f"{player.name} Hand: {player.hand_value}")
                            cls.__view_hand__(player)
                            total_busts += 1
                            break
                    if option == GameOption.Stand:
                        player_stand = True
                        break
                print()
        
        # __DEALER turn
        while True:
            if total_busts == len(players):
                break

            if cls.__DEALER.hand_value < 17:
                cls.__deal_to_player__(cls.__DEALER)
            else:
                break
            
            if (cls.__DEALER.hand_value > 20):
                break
        
        # payout
        print(f"Dealer Hand: {cls.__DEALER.hand_value}")
        cls.__view_hand__(cls.__DEALER)
        print()
        cls.__view_hands__(players)
        for player in players:
            if player.blackjack:
                player.blackjack = False
                continue
            if player.hand_value > 21 or cls.__DEALER.hand_value > 21:
                if player.hand_value > 21:
                    print(f"{player.name} busted")
                    player.add_money(cls.__bets[player.name] * -1)
                else:
                    print(f"Dealer busted, {player.name} wins")
                    player.add_money(cls.__bets[player.name])
            elif player.hand_value > cls.__DEALER.hand_value:
                print(f"{player.name} wins")
                player.add_money(cls.__bets[player.name])
            elif player.hand_value < cls.__DEALER.hand_value:
                print(f"{player.name} loses")
                player.add_money(cls.__bets[player.name] * -1)
            else:
                print("Tie")
                player.add_money(cls.__bets[player.name] * 0)
            try:
                input()
            except:
                print("Let's keep it going.")
        
        cls.__game_over__(players)

    @classmethod
    def __set_bet__(cls, player: BlackjackPlayer):
        """Function to set a player's bet
        
        Parameter
        ---
        player : BlackjackPlayer
            The player to set their bet
        """
        print(f"{player.name} ({player.money_pretty}), please set your bet")
        while True:
            money: float
            try:
                money = float(input())
            except:
                print("Nah bruh, we in this. Set your bet.")
                continue

            if money >= cls.__BET_MINIMUM and money <= player.money:
                cls.__bets[player.name] = money
                return

    @classmethod
    def __view_hands__(cls, players: list[BlackjackPlayer]):
        """Function to view all players' hands
        
        Parameter
        ---
        player : list[BlackjackPlayer]
            The group of players in this round of blackjack
        """
        for player in players:
            print(f"{player.name} Hand: {player.hand_value}")
            player.see_hand_horizon()
            print()

    @classmethod
    def __view_hand__(cls, player: BlackjackPlayer):
        """Function to view a player's hand
        
        Parameter
        ---
        player : BlackjackPlayer
            The group of players in this round of blackjack
        """
        for card in player.hand:
            print(card)

    @classmethod
    def __game_over__(cls, players: list[BlackjackPlayer]):
        """Function to reset all player's hands
        
        Parameter
        ---
        player : list[BlackjackPlayer]
            The group of players in this round of blackjack
        """
        print()
        cls.__DEALER.reset_hand()
        for player in players:
            player.reset_hand()

    @classmethod
    def __deal_card__(cls, number_of_cards: int):
        """Function to deal cards to all players and the __DEALER
        
        Parameter
        ---
        number_of_cards : int
            The number of cards to deal to each players
        """
        # do not deal if there are fewer cards available than requested
        for i in range(number_of_cards):
            if len(cls.__deck) < number_of_cards * len(cls.__players):
                return
            
            cls.__deal_to_player__(cls.__DEALER)
            for player in cls.__players.values():
                cls.__deal_to_player__(player)

    @classmethod
    def __deal_to_player__(cls, player: BlackjackPlayer):
        """Function to deal the next card to a player
        
        Parameter
        ---
        player : list[BlackjackPlayer]
            The player receiving the card
        """
        player.add_to_hand(cls.__deck[0])
        cls.__deck.pop(0)

    @classmethod
    def __more_players__(cls):
        """Adds additional players to the round"""
        while True:
            try:
                option = cls.__get_start_option__("\nWould anyone else like to play?", "0. New Game\n1. Continue\n2. No\n")
            except:
                option = StartOption.No

            if option == StartOption.NewGame:
                if cls.__new_character__():
                    break
            elif option == StartOption.Continue:
                if cls.__return_character__():
                    break
            elif option == StartOption.No:
                return
            else:
                return
        
        # recursion for more players
        if len(cls.__players) < 4:
            cls.__more_players__()

    @classmethod
    def __get_start_option__(cls, message: str, options: str) -> StartOption:
        """Returns a StartOption based a response from a prompt
        
        Parameters
        ---
        message : str
            A question asking why the user is selecting which StartOption
        options : str
            Set of StartOptions from which to choose
        """
        option_string: str
        print(message)
        while True:
            option_string = input(options)
            if (Common.is_int(option_string)):
                try:
                    return StartOption(int(option_string))
                except:
                    continue
            else:
                try:
                    return StartOption[option_string.replace(" ", "")]
                except:
                    print(f"Invalid command: {option_string}")
                    continue

    @classmethod
    def __get_game_option__(cls) -> GameOption:
        """Returns a GameOptions"""
        option_string: str
        while True:
            option_string = input("1. Hit\n2. Stand\n")
            if (Common.is_int(option_string)):
                try:
                    return GameOption(int(option_string))
                except:
                    continue
            else:
                try:
                    return GameOption[option_string]
                except:
                    print(f"Invalid command: {option_string}")
                    continue

    @classmethod
    def __new_character__(cls, firstChar: bool = False) -> bool:
        """Adds a new player to player dictionary
        
        Parameter
        ---
        firstChar : bool
            Whether or not this is the first player character
        """
        name: str = ""
        while True:
            try:
                name = input(f"\nWhat's your name? " + ("" if firstChar else "(just press Enter to return to the previous page) ")).lower()
            except:
                # cancel out of function
                break
            if cls.__players.get(name) != None:
                print("We already have someone in with that name.")
                continue
            break
        if not name:
            return False
        password = input("And the password> ")
        player = BlackjackPlayer(name, 500)
        cls.__players[name] = player
        cls.__saveData[name] = {"pass": HashClass.convert(cls.__KEY, password)["hash"]}
        return True

    @classmethod
    def __return_character__(cls, firstChar: bool = False) -> bool:
        """Adds a returning player to player dictionary
        
        Parameter
        ---
        firstChar : bool
            Whether or not this is the first player character
        """
        player: BlackjackPlayer
        name: str = None
        while True:
            try:
                name = input(f"\nWhat's your name, again? " + ("" if firstChar else "(just press Enter to return to the previous page) ")).lower()
            except:
                # cancel out of function
                break
            if cls.__players.get(name):
                print("We already have someone in with that name.")
                continue
            break
        if not name:
            return False
        char = cls.__saveData.get(name)
        if char == None:
            print(f"No saved player with the name {name}.")
            # force user to retry if the character isn't found
            return False
        else:
            attempts = 0
            while attempts < 3:
                if input("What is your password> ") == HashClass.deconvert(cls.__KEY, cls.__saveData[name]["pass"]):
                    break
                print("Invalid password")
                attempts += 1
                if attempts == 3:
                    print("Returning to menu")
                    return False
            player = BlackjackPlayer(name, char["money"], char["earnings"])
        cls.__players[name] = player
        return True

    @classmethod
    def __account__(cls):
        """Changes a pre-existing player's password"""
        if not cls.__return_character__():
            return
        player: BlackjackPlayer = list(cls.__players.values())[0]
        cls.__saveData[player.name]["pass"] = HashClass.convert(cls.__KEY, input("What would you like to change your password to> "))["hash"]
        cls.__players.pop(player.name)
