from card_library.deck_model.card import Card
from card_library.deck_model.deck_builder import DeckBuilder
from blackjack_library.enums.start_option import StartOption
from blackjack_library.enums.game_option import GameOption
from blackjack_library.enums.game_state import GameState
from blackjack_library.player import BlackjackPlayer
from crypto.hash_test import HashClass
from utils.common import Common
from os import path
import json
import random
import sys
from uuid import *

class Blackjack:
    """Represents a game of Blackjack for blackjack_api"""

    def __init__(self, uuid: UUID = None):
        """Initializes a new game of blackjack
        
        Parameter
        ---
        uuid : UUID
            uuid for a previously created game
        """
        self.__bets: dict = {}
        self.__deck: list[Card] = DeckBuilder.shuffled_standard_deck
        self.__players: dict[str, BlackjackPlayer] = {}
        self.__saveData: dict = {}
        self.__BET_MINIMUM: int = 50
        self.__BALANCE_MINIMUM: int = 100
        self.__DEALER = BlackjackPlayer("__DEALER", 1000, 1000)
        self.__PNG_ROOT = ".\\pngs\\standard_deck"
        self.__KEY = 10
        self.__uuid = uuid or uuid4()
        self.__state: GameState = GameState.Empty
        self.__userTurn: int = 0
        self.__total_busts: int = 0
        self.valid = Common.validate_deck(self.__PNG_ROOT, self.__deck)
        if self.valid["result"]:
            self.__saveData = Common.load(self.uuid)

    @property
    def players(self) -> list[BlackjackPlayer]:
        """Gets the current player list"""
        return self.__players.values()

    @property
    def uuid(self) -> str:
        """Retuns the game's uuid"""
        return str(self.__uuid)

    @property
    def state(self) -> GameState:
        """Returns a game's state"""
        return self.__state

    def start(self) -> dict:
        """Main function to play blackjack"""
        if len(self.__players) > 0 and not self.__state == GameState.InGame:
            self.__state = GameState.InGame
            return {"message": "Game is ready to start"}
        
        return {"player_count": len(self.__players), "state": self.__state} 

    def add_player(self, name: str) -> dict:
        """Adds a new player to player dictionary
        
        Parameter
        ---
        name : str
            The name of the player to add
        """
        if len(self.__players) > 3:
            return {"message": "This game is full."}
        if not name:
            return {"message": "No name supplied"}
        if self.__players.get(name) != None or self.__saveData.get(name) != None:
            return {"message": "We already have someone in with that name."}
        player = BlackjackPlayer(name, 500)
        self.__players[name] = player
        self.__saveData[name] = {"money": player.money, "winnings": player.winnings, "in_use": True}
        if len(self.__players) > 0 and self.__state == GameState.Empty:
            self.__state = GameState.Queuing
        if len(self.__players) > 3 and self.__state != GameState.InGame:
            self.__state = GameState.InGame
        self.__autosave__()
        return self.__saveData[name]

    def remove_player(self, name: str) -> dict:
        """Adds a new player to player dictionary
        
        Parameter
        ---
        name : str
            The name of the player to add
        """
        if self.__state == GameState.InGame:
            return {"message": "This game is currently in session"}
        if not name:
            return {"message": "No name supplied"}
        player = self.__players.get(name)
        if not player:
            return {"message": f"No player here with name {name}"}
        self.__players.pop(name)
        del player
        player_data = self.__saveData.pop(name)
        self.__autosave__()
        return player_data

    def get_player(self, name: str) -> dict:
        """Returns a Blackjack player

        Parameter
        ---
        name : str
            The Blackjack player's name
        """
        return self.__saveData.get(name) or {"message": f"No player here with name {name}"}

    def set_bet(self, name: str, money: float) -> dict:
        """Function to set a player's bet
        
        Parameters
        ---
        name : str
            The player to set their bet
        money : float
            The money the player wants to bet
        """
        if len(self.__bets) == len(self.__players):
            return {"message": "We're no longer taking bets."}

        player = self.__players.get(name)
        if not player:
            return {"message": f"{name} is not in this round."}

        if self.__bets.get(name):
            return {"message": "{name} has already placed their bet."}

        if money >= self.__BET_MINIMUM and money <= player.money:
            self.__bets[name] = money
            self.__update_player_funds__(name, -1 * money)
            if len(self.__bets) == len(self.__players):
                if len(self.__deck) < 5 * len(self.__players):
                    self.__deck = DeckBuilder.shuffled_standard_deck
                self.__deal_cards__(2)
                self.__userTurn
            self.__autosave__()
            return {"message": "Success"}
        
        return {"message": "Invalid input.", "bet_minimum": self.__BET_MINIMUM, "player_funds": player.money}

    def deal_to_player(self, name: str, option: GameOption) -> dict:
        """Function to set a player's bet

        Parameters
        ---
        name : str
            The player who's turn it is
        option : GameOption
            Whether to Hit or Stand
        """
        if len(self.__bets) != len(self.__players):
            return {"message": "We haven't finished taking bets."}

        players = self.__players.values()
        requested_user = 0
        for player in players:

            if player.name == name:

                if requested_user == self.__userTurn:
                    if option == GameOption.Hit:
                        self.__deal_to_player__(player)
                    if option == GameOption.Stand or player.hand_value > 20:
                        self.__userTurn += 1
                    if player.hand_value > 21:
                        self.__total_busts += 1
                    break

                return {"message": f"It isn't {name}'s turn."}

            requested_user += 1
        
        if self.__userTurn > len(players):
            print(f"Dealer Hand: {self.__DEALER.hand_value}")
            self.__view_hand__(self.__DEALER)
            print()
            self.__view_hands__(players)
            for player in players:
                name = player.name
                if self.__DEALER.hand_value > 21:
                    if player.hand_value > 21:
                        print(f"{name} busted")
                        player.add_money(self.__bets[player.name] * -1)
                    else:
                        print(f"Dealer busted, {name} wins")
                        self.__update_player_funds__(name, 2 * self.__bets[name])
                elif player.hand_value > self.__DEALER.hand_value:
                    print(f"{name} wins")
                    self.__update_player_funds__(name, 2 * self.__bets[name])
                elif player.hand_value < self.__DEALER.hand_value:
                    print(f"{player.name} loses")
                else:
                    print(f"{name} Tie")
                    self.__update_player_funds__(name, self.__bets[name])
            self.__game_over__(players)

        return self.get_player(name)

    def __update_player_funds__(self, name: str, money: float):
        """Updates a requested player's funds
        
        Parameters
        ---
        name : str
            The name of the player
        money : float
            The money to add (or subtract)
        """
        self.__players[name].add_money(money)
        self.__saveData[name]["money"] = self.__players[name].money
        self.__saveData[name]["winnings"] = self.__players[name].winnings

    def __deal_cards__(self, number_of_cards: int):
        """Function to deal cards to all players and the __DEALER
        
        Parameter
        ---
        number_of_cards : int
            The number of cards to deal to each players
        """
        # do not deal if there are fewer cards available than requested
        players = self.__players.values()
        for i in range(number_of_cards):
            if len(self.__deck) < number_of_cards * len(self.__players):
                return
            
            self.__deal_to_player__(self.__DEALER)
            for player in players:
                self.__deal_to_player__(player)

        # blackjack check on players
        for player in players:
            if player.hand_value == 21:
                name = player.name
                if player.hand_value > self.__DEALER.hand_value:
                    print(f"{name} hit blackjack")
                    self.__update_player_funds__(name, 2.5 * self.__bets[name])
                    player.blackjack = True
                if player.hand_value == self.__DEALER.hand_value:
                    print(f"{name} Tie")
                    self.__update_player_funds__(name, self.__bets[name])

        # blackjack check on Dealer
        if self.__DEALER.hand_value == 21:
            self.__view_hands__(players)
            self.__view_hand__(self.__DEALER)
            print("Dealer hit blackjack")
            self.__game_over__(players)
            return

    def __deal_to_player__(self, player: BlackjackPlayer):
        """Function to deal the next card to a player
        
        Parameter
        ---
        player : BlackjackPlayer
            The player receiving the card
        """
        player.add_to_hand(self.__deck.pop(0))

    def __autosave__(self):
        """Saves and reloads the player data from save file"""
        Common.save(self.uuid, self.__saveData)
        self.__saveData = Common.load(self.uuid)

    def __view_hands__(self, players: list[BlackjackPlayer]):
        """Function to view all players' hands
        
        Parameter
        ---
        player : list[BlackjackPlayer]
            The group of players in this round of blackjack
        """
        for player in players:
            print(f"{player.name} Hand: {player.hand_value}")
            player.see_hand_horizon()
            print()

    def __view_hand__(self, player: BlackjackPlayer):
        """Function to view a player's hand
        
        Parameter
        ---
        player : BlackjackPlayer
            The group of players in this round of blackjack
        """
        for card in player.hand:
            print(card)

    def __game_over__(self, players: list[BlackjackPlayer]):
        """Function to reset all player's hands
        
        Parameter
        ---
        player : list[BlackjackPlayer]
            The group of players in this round of blackjack
        """
        print()
        self.__bets = {}
        self.__DEALER.reset_hand()
        for player in players:
            player.reset_hand()
