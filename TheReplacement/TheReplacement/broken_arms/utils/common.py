from card_library.deck_model.card import Card
from os import path
import json

class Common:
    """Provides a library of common functions throughout the package"""

    @classmethod
    def save(cls, uuid: str, data: dict):
        """Saves data in a savedata.json file

        Parameter
        ---
        data : dict
            The dictionary to save
        """
        saveFile = open(f".\__pycache__\{uuid}.json", "w+")
        saveFile.write(json.dumps(data, indent=4))
        saveFile.close()

    @classmethod
    def load(cls, uuid):
        """Loads data from a savedata.json file"""
        if path.exists(f'.\__pycache__\{uuid}.json'):
            with open(f'.\__pycache__\{uuid}.json') as f:
                return json.load(f)
        else:
            return {}

    @classmethod
    def is_int(cls, value: str) -> bool:
        """Return true if the value is an int
        
        Parameter
        ---
        value : str
            The value to check
        """
        try:
            int(value)
            return True
        except:
            return False

    @classmethod
    def validate_deck(cls, png_root: str, deck: list[Card]) -> dict:
        """Returns True if [deck] is a list of cards with png files
        
        Parameters
        ---
        png_root : str
            root path to the png folder
        deck : list[Card]
            the card deck to validate
        """
        is_valid = True
        message: str = ""
        for i in range(len(deck)):
            if not isinstance(deck[i], Card):
                message += f"{card} is not a valid Card\n"
                is_valid = False
            card_path = f"{png_root}\\{deck[i].png}"
            if not path.exists(card_path):
                message += f"Missing {card} in {png_root}\n"
                if is_valid:
                    is_valid = False
        
        return {"result": is_valid, "message": message}
