from card_library.deck_model.card import Card
from card_library.deck_model.deck_builder import DeckBuilder
from card_library.enums.value import Value

class BasePlayer:
    """Represent a player character"""
    
    def __init__(self, name: str, money: float, winnings: float = 0):
        """Initializes a new player character

        Parameters
        ---

        name : str
            The player's name
        money : float
            The player's current money
        winnings : float
            The player's current winning
        """
        self.name = name
        self.__money: float = money
        self.__total_winnings: float = winnings
        self.__hand: list[Card] = []
    
    @staticmethod
    def _pretty_money_(the_money: float) -> str:
        """Formats money in $X.XX"""
        money = str(the_money if the_money >= 0 else the_money * -1)
        thing: str
        if "." not in money:
            thing = f"{money}.00"
        else:
            decimalPlaces = len(money.split(".")[1])
            thing = f"{money}0" if decimalPlaces == 1 else f"{money}"
        return f"${thing}" if the_money >= 0 else f"$({thing})"
    
    @property
    def money(self) -> float:
        """Returns the player's money in $X.XX format"""
        return round(self.__money, 2)
    
    @money.setter
    def set_money(self, money: float):
        """Sets the player's money in $X.XX format

        Parameters
        ---
        money : float
            The amount of money to set the player's funds.
            This value is automatically rounds to the nearest cent.
            This value can be negative.
        
        Raises
        ---
        TypeError
            If the [money] parameter is neither an int or float
        """
        money_type = type(money)
        if not (money_type is int or money_type is float):
            raise TypeError(f"{money} is {money_type}. Please enter an int or float")
        self.__money = round(money, 2)
    
    @property
    def winnings(self) -> float:
        """Returns the player's total winnings in $X.XX format"""
        return round(self.__total_winnings, 2)
    
    @winnings.setter
    def set_winnings(self, winnings: float = 0):
        """Sets the player's winnings in $X.XX format

        Parameters
        ---
        winnings : float
            The amount of money the player has currently won/lost.
            This value is automatically rounds to the nearest cent.
            This value can be negative.
        
        Raises
        ---
        TypeError
            If the [winnings] parameter is neither an int or float
        """
        money_type = type(winnings)
        if not (money_type is int or money_type is float):
            raise TypeError(f"{winnings} is {money_type}. Please enter an int or float")
        self.__total_winnings = round(winnings, 2)
    
    @property
    def hand(self) -> list[Card]:
        """Returns the player's current hand"""
        return self.__hand
    
    @property
    def money_pretty(self) -> str:
        """Returns the player's account in $X.XX format"""
        return self._pretty_money_(self.__money)
    
    @property
    def winnings_pretty(self) -> str:
        """Returns the player's total winnings in $X.XX format"""
        return self._pretty_money_(self.__total_winnings)
    
    def add_money(self, money: float):
        """Adds (or subtracts) the user's money in $X.XX format

        Parameter
        ---
        money : float
            The amount of money to set the player's funds.
            This value is automatically rounds to the nearest cent.
            This value can be negative.
        
        Raises
        ---
        TypeError
            If the [money] parameter is neither an int or float
        """
        money_type = type(money)
        if not (money_type is int or money_type is float):
            raise TypeError(f"{money} is {money_type}. Please enter an int or float")
        self.__money += money
        self.__total_winnings += money
    
    def add_to_hand(self, card: Card):
        """Adds a card to the player's hand
        
        Parameter
        ---
        card : Card
            The card to add to the player's hand
        
        Raises
        ---
        TypeError
            If the [card] parameter is not a Card
        """
        if not isinstance(card, Card):
            raise TypeError(f"{card} is not a valid Card")
        self.__hand.append(card)
    
    def see_hand(self):
        """Prints each card in the user's hand"""
        for card in self.__hand:
            print(card)
    
    def see_hand_horizon(self):
        """Returns the player's hand in a single line"""
        result: str = ""
        for card in self.__hand:
            result += f"{card} | "
        print(result)
    
    def reset_hand(self):
        """Removes all cards from the user's hand"""
        self.__hand = []
