from utils.player_base import BasePlayer
from card_library.enums.value import Value

class BlackjackPlayer(BasePlayer):
    """Represents a blackjack player"""

    def __init__(self, name: str, money: float, winnings: float = 0):
        """Initializes a new blackjack player

        Parameters
        ---

        name : str
            The player's name
        money : float
            The player's current money
        winnings : float
            The player's current winning
        """
        super().__init__(name, money, winnings)
        self.blackjack = False

    @property
    def hand_value(self) -> int:
        """Return the blackjack value of the player's hand"""
        temp = 0
        aces = 0
        for card in self.hand:
            card_value: Value = card.value
            if card_value == Value.Ace:
                aces += 1
            else:
                value = 10 if card_value.value > 10 else card_value.value
                temp += value
        
        for i in range(aces):
            if temp + 11 > 21:
                temp += 1
            else:
                temp += 11
        
        return temp
