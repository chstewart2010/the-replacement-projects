from enum import IntEnum

class GameOption(IntEnum):
    Hit = 1
    Stand = 2
