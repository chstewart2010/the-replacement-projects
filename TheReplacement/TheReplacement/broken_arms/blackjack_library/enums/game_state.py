from enum import IntEnum

class GameState(IntEnum):
    Queuing = 1
    InGame = 2
    Empty = 3
