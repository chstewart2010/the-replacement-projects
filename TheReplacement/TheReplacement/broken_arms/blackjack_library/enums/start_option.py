from enum import IntEnum

class StartOption(IntEnum):
    NewGame = 0
    Continue = 1
    No = 2
    Account = 3
