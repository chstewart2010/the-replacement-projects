﻿using Microsoft.EntityFrameworkCore;
using TheReplacement.Pages.Charles.PokemonSim;

namespace TheReplacement.Data
{
    public class TheReplacementContext : DbContext
    {
        public TheReplacementContext (DbContextOptions<TheReplacementContext> options)
            : base(options)
        {
        }

        public DbSet<GameData> GameData { get; set; }
    }
}
