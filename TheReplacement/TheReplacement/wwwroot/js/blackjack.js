﻿/** Blackjack root */
const BlackjackRoot = 'http://localhost:5000/api/v1/resources/blackjack'
/**Collection of valid BlackJack URIs */
const BlackjackURIs = {
    /**URI to GET, POST, or DELETE a Blackjack game*/
    Create: BlackjackRoot,
    /**POST URI to start Blackjack game, must have at least one active player to start*/
    Start: `${BlackjackRoot}/start`,
    /**POST URI to add a Blackjack player, must not match the name of an existing player in the same game*/
    AddPlayer: `${BlackjackRoot}/add`,
    /**POST URI to set a Blackjack player's bet, must be >= 50 and <= player's current funds*/
    SetBet: `${BlackjackRoot}/bet`,
    /**POST URI to deal to a Blackjack player, must be the player's turn*/
    Deal: `${BlackjackRoot}/deal`,
}

var game;

$("#new-game-btn").bind("click", function () {
    $.ajax({
        url: BlackjackURIs.Create,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        type: "POST",
        dataType: "json",
        data: null,
        success: function (result) {
            game = result
        },
        error: function () {
            console.log("error");
        }
    });
})