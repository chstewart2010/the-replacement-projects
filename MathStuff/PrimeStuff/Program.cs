﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeStuff
{
    class Program
    {
        static void Main(string[] args)
        {
            string line;
            while ((line = Console.ReadLine().Trim()) != null)
            {
                if (!ulong.TryParse(line, out var result) || result < 1)
                {
                    Console.WriteLine($"n must be greater than 0");
                    continue;
                }

                var primeResult = IsPrime(result) ? "is" : "is not";
                Console.WriteLine($"Factors of {result}: {string.Join(",", FactorsOfN(result))}");
                Console.WriteLine($"{result} {primeResult} a prime number");
                string factorization = "1 * ";
                foreach (var factor in PrimeFactorization(result))
                {
                    var exponent = factor.Value == 1 ? string.Empty : $"^{factor.Value}";
                    factorization += $"{factor.Key}{exponent} * ";
                }

                Console.WriteLine(factorization.TrimEnd(' ', '*'));
            }
        }

        static IEnumerable<ulong> FactorsOfN(ulong n)
        {
            if (n < 1)
            {
                throw new ArgumentException($"n must be greater than 0", nameof(n));
            }

            var factors1 = new List<ulong>();
            var factors2 = new List<ulong>();
            for (ulong i = 1; i <= Math.Sqrt(n); i++)
            {
                if (n % i == 0)
                {
                    factors1.Add(i);
                    if (i * i != n)
                    {
                        factors2.Add(n / i);
                    }
                }
            }
            factors2.Reverse();
            factors1.AddRange(factors2);
            return factors1;
        }

        static Dictionary<ulong, int> PrimeFactorization(ulong n)
        {
            if (n < 1)
            {
                throw new ArgumentException($"n must be greater than 0", nameof(n));
            }

            if (n == 1)
            {
                return new Dictionary<ulong, int>()
                {
                    {1, 1}
                };
            }

            var factorization = new Dictionary<ulong, int>();
            ulong i = 2;
            while (n > 1)
            {
                while (n % i == 0)
                {
                    if (factorization.ContainsKey(i))
                    {
                        factorization[i]++;
                    }
                    else
                    {
                        factorization[i] = 1;
                    }
                    n /= i;
                }
                i++;
            }

            return factorization;
        }

        static bool IsPrime(ulong n)
        {
            if (n < 1)
            {
                throw new ArgumentException($"n must be greater than 0", nameof(n));
            }

            for (ulong i = 2; i <= Math.Sqrt(n); i++)
            {
                if (n % i == 0)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
