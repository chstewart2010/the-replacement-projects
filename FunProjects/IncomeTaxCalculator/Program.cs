﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncomeTaxCalculator
{
    class Program
    {
        enum TaxBracket
        {
            A = 10,
            B = 12,
            C = 22,
            D = 24,
            E = 32,
            F = 35,
            G = 37
        }

        enum TaxBracket2
        {
            A = 0,
            B = 15,
            C = 20
        }

        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("Enter a dollar amount> ");
                if (!(double.TryParse(Console.ReadLine(), out var money) && money > 0))
                {
                    continue;
                }

                Console.WriteLine($"$ {Math.Round(MyFunc2(money, TaxBracket2.A), 2)}");
            }
        }

        static double MyFunc2(double moneyLeft, TaxBracket2 bracket, double currentlyTaxed = 0)
        {
            double bracketLimit = bracket switch
            {
                TaxBracket2.A => 40000 - 0,
                TaxBracket2.B => 441450 - 40000,
                TaxBracket2.C => moneyLeft,
            };

            var incomeNotTaxed = moneyLeft - bracketLimit;
            var bracketVal = (int)bracket;
            TaxBracket2 nextBracket = default;
            if (incomeNotTaxed > 0)
            {
                nextBracket = bracket switch
                {
                    TaxBracket2.A => TaxBracket2.B,
                    TaxBracket2.B => TaxBracket2.C,
                };
            }

            var additional = incomeNotTaxed <= 0
                ? moneyLeft * bracketVal / 100
                : MyFunc2(incomeNotTaxed, nextBracket, bracketLimit * bracketVal / 100);
            return currentlyTaxed + additional;
        }

        static double MyFunc(double moneyLeft, TaxBracket bracket, double currentlyTaxed = 0)
        {
            double bracketLimit = bracket switch
            {
                TaxBracket.A => 9950 - 0,
                TaxBracket.B => 40525 - 9950,
                TaxBracket.C => 86375 - 40525,
                TaxBracket.D => 164925 - 86375,
                TaxBracket.E => 209425 - 164925,
                TaxBracket.F => 523600 - 209425,
                TaxBracket.G => moneyLeft,
            };

            var incomeNotTaxed = moneyLeft - bracketLimit;
            var bracketVal = (int)bracket;
            TaxBracket nextBracket = default;
            if (incomeNotTaxed > 0)
            {
                nextBracket = bracket switch
                {
                    TaxBracket.A => TaxBracket.B,
                    TaxBracket.B => TaxBracket.C,
                    TaxBracket.C => TaxBracket.D,
                    TaxBracket.D => TaxBracket.E,
                    TaxBracket.E => TaxBracket.F,
                    TaxBracket.F => TaxBracket.G,
                };
            }

            var additional = incomeNotTaxed <= 0
                ? moneyLeft * bracketVal / 100
                : MyFunc(incomeNotTaxed, nextBracket, bracketLimit * bracketVal / 100);
            return currentlyTaxed + additional;
        }
    }
}
