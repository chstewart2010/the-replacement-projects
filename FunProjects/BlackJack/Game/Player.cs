﻿using CardLibrary.DeckModel;
using CardLibrary.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack.Game
{
    class Player
    {
        private double _money = 0;

        public double Money
        {
            get => Math.Round(_money, 2);
            set => _money = Math.Round(value, 2);
        }

        public string MoneyInPretty
        {
            get
            {
                var moneyString = Money.ToString();
                if (!moneyString.Contains("."))
                {
                    return $"${moneyString}.00";
                }

                var decimalPlaces = moneyString.Split('.')[1].Length;
                return decimalPlaces == 1 ? $"${moneyString}0" : $"${moneyString}";
            }
        }

        public List<Card> Hand { get; private set; } = new List<Card>();

        public int HandValue
        {
            get
            {
                int temp = 0;
                var aces = 0;
                foreach (var card in Hand)
                {
                    if (card.Value == Value.Ace)
                    {
                        aces++;
                    }
                    else
                    {
                        temp += (int)card.Value;
                    }
                }

                for (int i = 0; i < aces; i++)
                {
                    if (temp + 11 > 21)
                    {
                        temp++;
                    }
                    else
                    {
                        temp += 11;
                    }
                }

                return temp;
            }
        }

        public void SeeHand()
        {
            foreach (var card in Hand)
            {
                Console.WriteLine(card);
            }
        }

        public void ResetHand()
        {
            Hand = new List<Card>();
        }
    }
}
