﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack.Enums
{
    public enum Option
    {
        Quit = 1,
        NewGame,
        Continue,
        Hit,
        Stand
    }
}
