﻿using BlackJack.Enums;
using BlackJack.Game;
using CardLibrary.DeckModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack
{
    class Program
    {
        private static double _bet;
        private static uint _betMinimum;
        private static uint _balanceMinimum;
        private static IList<Card> Deck;
        private static readonly Player Player = new Player();
        private static readonly Player Dealer = new Player();
        private static readonly Random Rng = new Random();
        static void Main(string[] args)
        {
            var largeNum = "";
            var val = double.MaxValue;
            while (val > 0)
            {
                largeNum += (val % 10).ToString();
                val -= val % 10;
                val /= 10;
            }
            var correct = "";
            for (int i  = 1; i <= largeNum.Length; i++)
            {
                correct += largeNum[largeNum.Length - i];
            }
            Console.WriteLine(correct);
            SetHouseRules();
            Option option;
            while (true)
            {
                Console.WriteLine($"Player Balance: {Player.MoneyInPretty}");
                Console.WriteLine("New Game\nContinue\nQuit\n");
                Enum.TryParse(Console.ReadLine().Replace(" ", ""), true, out option);
                switch (option)
                {
                    case Option.NewGame:
                        Deck = DeckBuilder.GetRandomDeck();
                        SetBalance();
                        Play();
                        break;
                    case Option.Continue:
                        if (Deck == null)
                        {
                            Console.WriteLine();
                            continue;
                        }
                        if (Player.Money < 50)
                        {
                            Console.WriteLine(Player.Money >= 0 ? "Bruh, you broke." : "Bruh, you in debt.");
                            continue;
                        }
                        Play();
                        break;
                    case Option.Quit:
                        return;
                    default:
                        Console.WriteLine($"Invalid Command {option}");
                        break;
                }
            }
        }

        static void Play()
        {
            SetBet();
            Deal(2);
            if (Player.HandValue == 21 || Dealer.HandValue == 21)
            {
                Console.WriteLine();
                ViewHands();
                if (Player.HandValue > Dealer.HandValue)
                {
                    Console.WriteLine("Player wins");
                    Player.Money += _bet * 1.5;
                }
                else if (Dealer.HandValue > Player.HandValue)
                {
                    Console.WriteLine("Dealer wins");
                    Player.Money -= _bet;
                }
                else
                {
                    Console.WriteLine("Tie");
                    Player.Money -= _bet;
                }
                GameOver();
                return;
            }
            Option option;
            bool breakCondition = false;
            bool playerStand = false;
            Console.WriteLine();

            //player turn
            while (true)
            {
                if (!playerStand)
                {
                    Console.WriteLine($"\nPlayer Hand: {Player.HandValue}");
                    Player.SeeHand();
                    Console.WriteLine("\nDealer Hand");
                    Console.WriteLine(Dealer.Hand[0] + "\n");
                    Console.WriteLine("\nHit\nStand\n");
                    Enum.TryParse(Console.ReadLine(), true, out option);
                    switch (option)
                    {
                        case Option.Hit:
                            Deal(Player);
                            if (Player.HandValue > 20)
                            {
                                breakCondition = true;
                            }
                            break;
                        case Option.Stand:
                            playerStand = true;
                            break;
                    }
                }

                Console.WriteLine();
                if (breakCondition || playerStand)
                {
                    break;
                }
            }

            //dealer turn
            while (true)
            {
                if (breakCondition)
                {
                    break;
                }

                if (Dealer.HandValue < 17)
                {
                    Deal(Dealer);
                }
                else if (playerStand)
                {
                    break;
                }

                if (Dealer.HandValue > 20)
                {
                    break;
                }
            }

            ViewHands();
            if (Player.HandValue > 21 || Dealer.HandValue > 21)
            {
                if (Player.HandValue > 21)
                {
                    Console.WriteLine("Dealer wins");
                    Player.Money -= _bet;
                }
                else
                {
                    Console.WriteLine("Player wins");
                    Player.Money += _bet;
                }
            }

            else if (Player.HandValue > Dealer.HandValue)
            {
                Console.WriteLine("Player wins");
                Player.Money += _bet;
            }

            else if (Dealer.HandValue > Player.HandValue)
            {
                Console.WriteLine("Dealer wins");
                Player.Money -= _bet;
            }

            else
            {
                Console.WriteLine("Tie");
            }

            GameOver();
        }

        static void SetHouseRules()
        {
            Console.WriteLine("Please set your Balance Minimum");
            while (true)
            {
                if (uint.TryParse(Console.ReadLine().Trim(), out var money) && money >= 1)
                {
                    _balanceMinimum = money;
                    break;
                }
            }
            Console.WriteLine("Please set your Bet Minimum");
            while (true)
            {
                if (uint.TryParse(Console.ReadLine().Trim(), out var money) && money >= 1 && money <= _balanceMinimum)
                {
                    _betMinimum = money;
                    return;
                }
            }
        }

        static void SetBalance()
        {
            Console.WriteLine("Please set your Balance");
            while (true)
            {
                if (double.TryParse(Console.ReadLine().Trim(), out var money) && money >= _balanceMinimum)
                {
                    Player.Money = money;
                    return;
                }
            }
        }

        static void SetBet()
        {
            Console.WriteLine("Please set your bet");
            while (true)
            {
                if (uint.TryParse(Console.ReadLine().Trim(), out var money) && money >= _betMinimum && money <= Player.Money)
                {
                    _bet = money;
                    return;
                }
            }
        }

        static void ViewHands()
        {
            Console.WriteLine($"Player Hand: {Player.HandValue}");
            Player.SeeHand();
            Console.WriteLine($"\nDealer Hand: {Dealer.HandValue}");
            Dealer.SeeHand();
            Console.WriteLine();
        }

        static void GameOver()
        {
            Console.WriteLine();
            Player.ResetHand();
            Dealer.ResetHand();
            Deck = DeckBuilder.GetRandomDeck();
        }

        static void Deal(int numberOfCards)
        {
            for (int i = 0; i < numberOfCards; i++)
            {
                if (Deck.Count < 2)
                {
                    return;
                }
                Deal(Player);
                Deal(Dealer);
            }
        }

        static void Deal(Player player)
        {
            int position = Rng.Next(Deck.Count);
            player.Hand.Add(Deck[position]);
            Deck.RemoveAt(position);
        }
    }
}
