﻿using CardLibrary.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardLibrary.DeckModel
{
    public class Card
    {
        internal Card(Value value, Suit suit)
        {
            Value = value;
            Suit = suit;
        }

        public Value Value { get; }
        public Suit Suit { get; }

        public override string ToString()
        {
            return $"{Value} of {Suit}";
        }
    }
}
