﻿using CardLibrary.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardLibrary.DeckModel
{
    public static class DeckBuilder
    {
        private static readonly Random Rng = new Random();

        public static IList<Card> GetRandomDeck()
        {
            return GetShuffle(GetDeck());
        }

        public static IList<Card> GetShuffle(IList<Card> deck)
        {
            List<Card> newDeck = new List<Card>();
            for (int i  = deck.Count() - 1; i >= 0; i--)
            {
                int position = Rng.Next(deck.Count);
                newDeck.Add(deck[position]);
                deck.RemoveAt(position);
            }

            return newDeck;
        }

        private static IList<Card> GetDeck()
        {
            List<Card> deck = new List<Card>();
            foreach (Value value in Enum.GetValues(typeof(Value)))
            {
                foreach (Suit suit in Enum.GetValues(typeof(Suit)))
                {
                    deck.Add(new Card(value, suit));
                }
            }

            return deck;
        }
    }
}
