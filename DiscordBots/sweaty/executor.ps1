
function Start-Function {
	param(
		[Parameter((Mandatory=$true))]
		[string]$FileName,
		[string]$Arguments
	)
	<#
		.Description
		Overloads the Start-Process command with the -NoNewWindow, -PassThru, and -RedirectStandardOutput switches

		.Parameter FileName
		The full path to the process to start. Manadatory

		.Parameter Arguments
		The arguments to pass when starting the process. Optional

		.Inputs
		None.

		.Outputs
		System.Diagnostics.Process. Start-Function starts the process and returns the object.

		.Example
		PS> Start-Function "notepad"

		.Example
		PS> Start-Function "notepad" "C:\file.txt"

		.Example
		PS> Start-Function -filename "notepad" -arguments "C:\file.txt"
	#>
	return Start-Process -FilePath $FileName -ArgumentList $Arguments -PassThru -NoNewWindow -RedirectStandardOutput "audit.log"
}

# run installation on project dependencies
$proc = Start-Function "npm" "i get-dependencies";
$proc.WaitForExit();
try {
	while ($proc.HasExited){
		# Ensure sweaty remains active
		Write-Host "Starting Sweaty Bot v1";
		$proc = Start-Function "node" "bot.js";
		$proc.WaitForExit();
	}
}
catch {
	# terminate script if some uncaught exception occurs
	Write-Host Stopping due to a critical error $_.ScriptStackTrace
}