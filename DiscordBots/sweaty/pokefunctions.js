var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

/**
 * Returns the url for the shiny pokemon's sprite
 * @param {String} pokemon - The pokemon to search for
 */
exports.getShiny = function (pokemon){
	var response = GetResponse(`pokemon/${pokemon}`)
	if (response == 'Not Found'){
		return 'Nope! I couldn\'t find anything!'
	}
	var json = JSON.parse(response)
	
	return json.sprites.front_shiny
}

/**
 * Returns a collection of pokemon who get an ability
 * @param {String} ability - The ability to search for
 */
exports.getAbility = function (ability){
	var response = GetResponse(`ability/${ability}`)
	if (response == 'Not Found'){
		return 'Nope! I couldn\'t find anything!'
	}
	var json = JSON.parse(response)

	return json.effect_entries[0].effect.toUpperCase()
}

/**
 * Return all egg groups that a pokemon fits in
 * @param {String} pokemon - The pokemon to search for
 * @returns 
 */
exports.getEggGroup = function (pokemon){
	var response = GetResponse(`pokemon-species/${pokemon}`)
	if (response == 'Not Found'){
		return 'Nope! I couldn\'t find anything!'
	}
	
	var json = JSON.parse(response)
	var groups = json.egg_groups
	var message =  `${pokemon.toUpperCase()} is in the ${groups[0].name.toUpperCase()}`
	if (groups.length == 1){
		message += ' egg group'
	}
	else{
		message += ` and ${groups[1].name.toUpperCase()} egg groups`
	}

	return message
}

/**
 * Returns a possible parents for a pokemon, useful for learning egg moves
 * @param {String} pokemon - The pokemon to search for
 * @param {String} move - The move to search for
 * @returns 
 */
exports.getPossibleParents = function (pokemon,move){
	//ensure pokemon actually gets the move
	var responseSpecies = GetResponse(`pokemon-species/${pokemon}`)
	if (responseSpecies == 'Not Found'){
		return 'Nope! I couldn\'t find anything!'
	}

	var jsonSpecies = JSON.parse(responseSpecies)
	var url = jsonSpecies.evolution_chain.url
	var request = new XMLHttpRequest()
	request.open("GET", url, false)
	request.send(null)
	var responseEvoChain = request.responseText

	if (responseEvoChain == 'Not Found'){
		return `What on earth is a ${pokemon}?`
	}
	var jsonEvoChain = JSON.parse(responseEvoChain)
	var firstForm = jsonEvoChain.chain.species.name
	var responseChild = GetResponse(`pokemon/${firstForm}`)
	var jsonResponse = JSON.parse(responseChild)
	var movesChild = jsonResponse.moves
	var wrongMove = true
	for (i = 0; i < movesChild.length; i++){
		if (movesChild[i].version_group_details[0].move_learn_method.name == 'egg' && movesChild[i].move.name == move){
			wrongMove = false
			break
		}
	}

	if (wrongMove){
		return `Sorry, but ${pokemon} doesn\'t even learn ${move} as an egg move`
	}

	// get egg groups
	pokemon = pokemon.toUpperCase()
	var message =  `${pokemon} learns ${move} from the following parents:\n`		
	var len1 = message.length
	var groups = jsonSpecies.egg_groups

	//search each egg group
	for (i = 0; i < groups.length; i++){
		var responseGroup = GetResponse(`egg-group/${groups[i].name}`)
		if (responseGroup == 'Not Found'){
			return 'Nope! I couldn\'t find anything!'
		}

		var jsonGroup = JSON.parse(responseGroup)
		var species = jsonGroup.pokemon_species
		for(j = 0; j < species.length; j++){
			var parent = species[j].name
			var responseParent = GetResponse(`pokemon/${parent}`)
			var jsonParent = JSON.parse(responseParent)
			var moves = jsonParent.moves
			for (k = 0; k < moves.length; k++){
				var method = moves[k].version_group_details[0].move_learn_method.name
				if (moves[k].move.name == move){
					message += `From ${parent.toUpperCase()} by ${method.toUpperCase()}`
					if (method == 'level-up'){
						message += ` at ${moves[k].version_group_details[0].level_learned_at}`
					}
					message += '\n'
				}
			}
		}
	}

	var len2 = message.length
	if (len1 == len2){
		return `${pokemon} doesn\'t learn ${move} as an egg move'`
	}

	return message
}

/**
 * Returns a collection of moves that a pokemon learn
 * @param {String} typeMove - The type of move to search for
 * @param {String} pokemon - The pokemon to search for
 * @returns 
 */
exports.getMoves = function (typeMove,pokemon){
	var type = typeMove.substring(0, typeMove.length - 6)
	var response = null

	if (type == 'egg'){
		var responseSpecies = GetResponse(`pokemon-species/${pokemon}`)
	
		if (responseSpecies == 'Not Found'){
			return 'Nope! I couldn\'t find anything!'
		}
		var jsonSpecies = JSON.parse(responseSpecies)
		var url = jsonSpecies.evolution_chain.url
		var request = new XMLHttpRequest()
		request.open("GET", url, false)
		request.send(null)
		var responseEvoChain = request.responseText
	
		if (responseEvoChain == 'Not Found'){
			return 'Nope! I couldn\'t find anything!'
		}
		var jsonEvoChain = JSON.parse(responseEvoChain)
		var firstForm = jsonEvoChain.chain.species.name
		response = GetResponse(`pokemon/${firstForm}`)
	}
	else{
		response = GetResponse(`pokemon/${pokemon}`)
	}
	
	if (response == 'Not Found'){
		return 'Nope! I couldn\'t find anything!'
	}
	
	pokemon = pokemon.toUpperCase()
	var json = JSON.parse(response)
	var moves = json.moves
	var message = `${pokemon} learns the following ${type} moves:\n`
	
	var len1 = message.length
	for (i = 0; i < moves.length; i++){
		var method = moves[i].version_group_details[0].move_learn_method.name
		if (method == type){
			message += moves[i].move.name.replace('-', ' ').toUpperCase()
			if (method == 'level-up'){
				message += ` at ${moves[i].version_group_details[0].level_learned_at}`
			}
			message += '\n'
		}
	}
	var len2 = message.length

	if (len1 == len2){
		return `${pokemon} doesn't learn any ${type} moves`
	}

	return message
}

/**
 * Returns information for the requested pokemon
 * @param {String} pokemon - The pokemon to search for
 * @returns 
 */
exports.getPokemon = function (pokemon){
	var response = GetResponse(`'pokemon/${pokemon}`)
	if (response == 'Not Found'){
		return 'Nope! I couldn\'t find anything!'
	}

	var json = JSON.parse(response)
	pokemon = pokemon.toUpperCase()
    var message = `Infomation on ${pokemon}\n`
	var abilities = json.abilities
    for (i = 0; i < abilities.length; i++){
        if (abilities[i].is_hidden == true){
            message += 'HIDDEN ABILITY - '
        }
        else{
            message += 'ABILITY - '
        }
        message += `${abilities[i].ability.name.replace("-", " ").toUpperCase()}\n`
    }

	var forms = json.forms
    if (forms.length == 1){
        // SKIP
    }
    else {
        // TODO
    }

	var types = json.types
    message += `${pokemon}'s type`
    if (types.length == 1){
        message += ' is '
    }
    else {
        message += 's are '
    }
    for (i = 0; i < types.length; i++){
        message += types[i].type.name.toUpperCase()
        if (i < types.length - 1){
            message += ' and '
        }
    }
    message += '\n\n'

	var stats = json.stats
	message += `${pokemon}'s base stats are:\n`
	stats.forEach(stat => {
        var statName = stat.stat.name.toUpperCase()
        var statValue = stat.base_stat
        message += `${statName.toUpperCase()}: ${statValue}\n`
	});

    return message
}

/**
 * Returns a stringified JSON of the response
 * @param {String} endpoint - The PokeAPI endpoint to hit 
 */
function GetResponse(endpoint){
	var request = new XMLHttpRequest()
    request.open("GET", `https://pokeapi.co/api/v2/${endpoint}`, false)
	request.send(null)
    return request.responseText
}