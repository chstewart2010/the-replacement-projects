﻿var Discord = require('discord.io');
var logger = require('winston');
var auth = require('./auth.json');
var PokeFunctions = require('./pokefunctions.js');
var Chopra = require('wisdom-of-chopra');
var days = ["Sunday" , "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console, {
	colorize: true
});
logger.level = 'debug';
// Initialize Discord Bot
var bot = new Discord.Client({
	token: auth.token,
	autorun: true
});
bot.on('ready', function (evt) {
	logger.info('Connected');
	logger.info('Logged in as: ');
	logger.info(`${bot.username} - (${bot.id})`);
});
bot.on('message', function (user, userID, channelID, message, evt) {
	// Our bot needs to know if it will execute a command
	// It will listen for messages that will start with `!`
	if (message[0] == '!' && message.length > 1 && userID != bot.id) {
		logger.info(`${user} triggered with ${message}`)
		var args = message.substring(1).split(' ');
		var cmd = args[0];

		args = args.splice(1);
		var result = null
		try {			
			switch (cmd.toLowerCase()) {
				// !ping
				case 'ping':
					result = 'Pong!'
					break;
				case 'what':
					result = what()
					break;
				case 'shiny-chance':
					if (args.length < 1){
						result = 'Please enter a nonnegative number when using !shinychance'
					}
					else {
						result = shinyChance(args[0])
					}
					break;					
				case 'shiny-average':
					console.log(args[0])
					if (args.length < 1){
						result = 'Please enter a number between 0 and 100'
					}
					else {
						result = shinyAverage(args[0].replace('%', ''))
					}
					break;
				case 'pokemon':
					result = PokeFunctions.getPokemon(args[0])
					break;
				case 'ability':
					result = PokeFunctions.getAbility(args.join('-'))
					break;
				case 'egg-groups':
					result = PokeFunctions.getEggGroup(args[0])
					break;
				case 'egg-heritance':
					var pokemon = null;
					var move = null;
					for (i = 0; i < args.length; i++){
						var splits = args[i].split('=')
						if (splits[0] == 'pokemon' && splits.length == 2){
							pokemon = splits[1]
						}
						if (splits[0] == 'move' && splits.length == 2){
							move = splits[1]
						}
					}

					if (pokemon == null){
						result = 'But for which pokemon'
					}
					else if (move == null){
						result = 'But which move'
					} else{
						result = PokeFunctions.getPossibleParents(pokemon, move)
					}
					break;
				case 'level-up-moves':
					result = PokeFunctions.getMoves(cmd, args.join('-'))
					break;
				case 'wisdom-of-chopra':
					result = getChopraQuote();
					break;
				case 'egg-moves':
					result = PokeFunctions.getMoves(cmd, args.join('-'))
					break;
				case 'machine-moves':
					result = PokeFunctions.getMoves(cmd, args.join('-'))
					break;
				case 'tutor-moves':
					result = PokeFunctions.getMoves(cmd, args.join('-'))
					break;
				case 'shiny':
					result = PokeFunctions.getShiny(args[0])
					break;
				default:
					break;
			}
		} catch (error) {
			result = `Whoops! Had a bit of a kerfuffle there. D'Snitch-Protocol dictates I say that {user} triggered this error:\n`
			result += error
			logger.error(`${user} triggered ${error}`)
		}
		var recipient = cmd.toLowerCase() == 'egg-heritance' ? userID : channelID;
		if (result != null){
			bot.sendMessage({
				to: recipient,
				message: result
			})
		}
	}
	if (message.toLowerCase() == 'hey sweatybot'){
		logger.info(`${user} saluted`)
		bot.sendMessage({
			to: channelID,
			message: `Hey ${user}, how have you been?`
		})
	}
});

//math function

/**
 * Gives the chance of finding a shiny pokemon given a 1/512 chance
 * @param {Number} attempts - Thge number of attempts at the shiny pokemon
 */
function shinyChance(attempts){
	if (attempts < 0) {
		return 'Please enter a nonnegative number when using !shiny-chance'
	}

	var num = Math.round((1 - Math.pow((511/512), attempts))*10000)/100
	if (Number.isNaN(num)){
		return 'Please enter a nonnegative number when using !shiny-chance'
	}
	return `You've got like a ${num}% chance. Good luck!`
}

/**
 * Gives the average number of attempts requires to find a shiny pokemon within a given percentage
 * @param {Number} percent - Thge number of attempts at the shiny pokemon
 */
function shinyAverage(percent){
	if (percent < 0 || percent > 100){
		return 'Please enter a number between 0 and 100'
	}
	if (percent == 0 || percent == 100){
		return 'Please don\'t use 0 or 100'
	}

	var num = Math.round((Math.log(1 - percent/100)/Math.log(511/512)))
	if (Number.isNaN(num)){
		return 'Please enter a number between 0 and 100'
	}

	return `That'll take you about ${num} eggs on average`
}

//other functions

/**
 * @returns A random Deepak Chopra quote from the Wisdom of Chopra randomizer
 */
function getChopraQuote(){
	var message = `${Chopra.getQuote()}\n\t- Deepak Chopra`
	return message
}

/**
 * @returns An explanation of each command sweatybot support
 */
function what(){
	return `!ping - I'll yell Pong! back at you

!egg-groups {pokemon} - I'll tell you which egg group(s) a pokemon is in

!egg-inheritance pokemon={pokemon} move={move} - My most ambitious role! I will list every parent that can teach a pokemon a requested egg move.
WARNING: This may take a while, so I'll pm you the results

!egg-moves {pokemon} - I'll tell you what moves a pokemon learns from its parents

!level-up-moves {pokemon} - I'll tell you what moves a pokemon learns by level-up\t(I'll even tell you what level it learns it)

!machine-moves {pokemon} - I'll tell you what move a pokemon learns by tm/tr

!pokemon {pokemon} - I'll give you infomation on a pokemon

!shiny {pokemon} - I'll show you what the shiny pokemon looks like

!shiny-average {percentage} - I'll tell you how many eggs you'd need to hatch to have a {percentage} chance of hatching a shiny pokemon with a 1/512 chance
(It honestly just perform the !shiny-chance in reverse)

!shiny-chance {attempts} - I'll give you the chance of getting a shiny pokemon within {attempts} attempts with a 1/512 chance

!tutor-moves {pokemon} - I'll tell you what moves a pokemon learns by tutoring

!what - I'll explain what I do, but you probably knew that

!wisdom-of-chopra - I'll give a possibly real quote from philosopher Deepak Chopra`
}